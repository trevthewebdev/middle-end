import { ServiceHandler } from '../utils/api.utils';

export const getRegions = ServiceHandler({
  url: '/regions',
});

export const getRegion = ServiceHandler({
  url: '/regions/:id',
});

export const createRegion = ServiceHandler({
  url: '/regions',
  method: 'post',
});

export const updateRegion = ServiceHandler({
  url: '/regions/:id',
  method: 'patch',
});

export const deleteRegion = ServiceHandler({
  url: '/regions/:id',
  method: 'delete',
});
