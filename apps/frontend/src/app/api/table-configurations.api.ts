import { TableConfigurationEntityInterface } from '@middle-end/api';

import { ServiceHandler } from '../utils/api.utils';

export const getTableConfigurations = ServiceHandler<
  Request,
  { data: TableConfigurationEntityInterface[] }
>({
  url: '/table-configurations',
});

export const getTableConfiguration = ServiceHandler<
  Request,
  { data: TableConfigurationEntityInterface }
>({
  url: '/table-configurations/:id',
});

export const createTableConfiguration = ServiceHandler({
  url: '/table-configurations',
  method: 'post',
});

export const updateTableConfiguration = ServiceHandler({
  url: '/table-configurations/:id',
  method: 'patch',
});

export const deleteTableConfiguration = ServiceHandler({
  url: '/table-configurations/:id',
  method: 'delete',
});

export const getTableConfigurationFields = ServiceHandler({
  url: '/table-configurations/fields',
});
