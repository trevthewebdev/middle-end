import { ServiceHandler } from '../utils/api.utils';

export const getScopes = ServiceHandler({
  url: '/scopes',
});

export const getScope = ServiceHandler({
  url: '/scopes/:id',
});

export const createScope = ServiceHandler({
  url: '/scopes',
  method: 'post',
});

export const updateScope = ServiceHandler({
  url: '/scopes/:id',
  method: 'patch',
});

export const deleteScope = ServiceHandler({
  url: '/scopes/:id',
  method: 'delete',
});
