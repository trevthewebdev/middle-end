import { QueryClient } from '@tanstack/react-query';
import Axios, { AxiosResponse } from 'axios';
import jwtDecode from 'jwt-decode';

// import { enqueueGlobalSnackbar } from '../state/events/snackbar.bus';
import {
  ACCESS_TOKEN_NOT_REFRESHED_EVENT,
  ACCESS_TOKEN_REFRESHED_EVENT,
  AuthBus,
  getStoredTokens,
  storeTokens,
} from '../utils/token.utils';

const queryClient = new QueryClient();

const queryKey = ['temp', 'dude'];

const INSTANCE_CONFIG = {
  baseURL: 'http://localhost:6100/api',
  headers: {
    'Content-Type': 'application/json',
  },
};

const Instance = Axios.create(INSTANCE_CONFIG);

Instance.interceptors.request.use(
  (config) => {
    if (config.headers?.Authorization) {
      return config;
    }

    const tokens = getStoredTokens();

    config.headers!.Authorization = `Bearer ${tokens.accessToken}`;

    return config;
  },
  (error) => Promise.reject(error)
);

Instance.interceptors.response.use(
  (response) => response,
  (error) => {
    const { response, code } = error;

    if (code === 'ERR_NETWORK') {
      console.log('NEED TO SHOW THIS THING...');
      // enqueueGlobalSnackbar(
      //   `Unable to connect to our servers, please check your connection or try again later`,
      //   {
      //     title: 'You may be offline',
      //     variant: 'error',
      //     persist: true,
      //   }
      // );
    }

    if (response?.status === 401) {
      queryClient
        .fetchQuery<AxiosResponse>(queryKey, () => attemptRefresh(), {
          retry: 3,
        })
        .then((response: any) => {
          storeTokens({ access: response.data });
          AuthBus.publish(ACCESS_TOKEN_REFRESHED_EVENT);
        })
        .catch((error) => {
          const { accessToken } = getStoredTokens();
          const payload: any = jwtDecode(accessToken!);

          if (new Date().valueOf() > payload.exp * 1000) {
            AuthBus.publish(ACCESS_TOKEN_NOT_REFRESHED_EVENT);
          }
        });
    }

    return Promise.reject(error);
  }
);

export { Instance as BackendApi };

async function attemptRefresh() {
  const { refreshToken } = getStoredTokens();

  return Axios.post(`${INSTANCE_CONFIG.baseURL}/tokens/access`, {
    token: refreshToken,
  });
}
