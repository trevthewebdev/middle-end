import { SelectItem } from '@mantine/core';

export function asSelectOptionsTransformer(label: string, value = 'id') {
  return ({ data }: any): SelectItem[] => {
    try {
      return data.map((entity: any) => ({
        label: entity[label],
        value: entity[value],
      }));
    } catch (error: any) {
      throw new Error(error.message);
    }
  };
}
