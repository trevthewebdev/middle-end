import { ServiceHandler } from '../utils/api.utils';

export const getFormatters = ServiceHandler({
  url: '/formatters',
});

export const getFormatter = ServiceHandler({
  url: '/formatters/:id',
});

export const createFormatter = ServiceHandler({
  url: '/formatters',
  method: 'post',
});

export const updateFormatter = ServiceHandler({
  url: '/formatters/:id',
  method: 'patch',
});

export const deleteFormatter = ServiceHandler({
  url: '/formatters/:id',
  method: 'delete',
});
