import { AppShell, AppShellProps } from '@mantine/core';
import { NavbarSearch } from './modules/NavbarSearch';

interface Props extends AppShellProps {
  width?: number | string;
  maxWidth?: number | string;
  justify?: 'left' | 'right' | 'center';
}

export function ConsoleLayout({
  children,
  justify = 'center',
  width = 980,
  ...props
}: Props) {
  return (
    <AppShell padding="lg" navbar={<NavbarSearch />} {...props}>
      <div
        style={{
          display: 'flex',
          justifyContent: justify,
          width,
          paddingBottom: '3rem',
        }}
      >
        <div>{children}</div>
      </div>
    </AppShell>
  );
}
