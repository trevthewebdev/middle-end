import { PubSubModule } from '../lib/packages';

// const { REACT_APP_TOKEN_KEY } = process.env;
// const { REACT_APP_REFRESH_KEY } = process.env;

const REACT_APP_TOKEN_KEY = 'REACT_APP_TOKEN_KEY';
const REACT_APP_REFRESH_KEY = 'REACT_APP_REFRESH_KEY';

const ACCESS_KEY = REACT_APP_TOKEN_KEY || 'analytics';
const REFRESH_KEY = REACT_APP_REFRESH_KEY || 'gaf';

interface TokenParams {
  access?: string;
  refresh?: string;
}

export const DEVICE_KEY = 'befe-device';
export const SUCCESSFUL_AUTHENTICATION_EVENT =
  'SUCCESSFUL_AUTHENTICATION_EVENT';
export const ACCESS_TOKEN_REFRESHED_EVENT = 'ACCESS_TOKEN_REFRESHED_EVENT';
export const ACCESS_TOKEN_NOT_REFRESHED_EVENT =
  'ACCESS_TOKEN_NOT_REFRESHED_EVENT';

export const AuthBus = new PubSubModule();

export function storeTokens(tokens: TokenParams, rememberDevice?: boolean) {
  if (tokens.access) {
    sessionStorage.setItem(ACCESS_KEY, tokens.access);
  }

  if (rememberDevice && tokens.refresh) {
    localStorage.setItem(REFRESH_KEY, tokens.refresh);
    return;
  }

  if (tokens.refresh) {
    sessionStorage.setItem(REFRESH_KEY, tokens.refresh);
  }
}

export function getStoredTokens() {
  return {
    accessToken: sessionStorage.getItem(ACCESS_KEY),
    refreshToken: isDeviceRemembered()
      ? localStorage.getItem(REFRESH_KEY)
      : sessionStorage.getItem(REFRESH_KEY),
  };
}

export function destroyTokens() {
  if (isDeviceRemembered()) {
    sessionStorage.removeItem(ACCESS_KEY);
    localStorage.removeItem(REFRESH_KEY);
    return;
  }

  sessionStorage.removeItem(ACCESS_KEY);
  sessionStorage.removeItem(REFRESH_KEY);
}

export function isDeviceRemembered() {
  const remember = localStorage.getItem(DEVICE_KEY);
  return remember === '1';
}
