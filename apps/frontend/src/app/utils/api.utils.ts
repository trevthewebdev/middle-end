import { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios';
import qs from 'qs';

import { BackendApi } from '../api/backend.config';

/* types */
type ServiceArgument<D = any> =
  | string
  | number
  | Partial<AxiosRequestConfig<D>>;

const defaultStaticConfig: Partial<AxiosRequestConfig> = {
  method: 'get',
  paramsSerializer: standardQuerySerializer,
  transformRequest: [ignoreUndefineds],
};

/**
 * @name ServiceHandler
 * @description Creates and returns a static endpoint definition (function) for reuse.
 * @param staticConfig Any available axios config options
 * @param instance A valid axios instance
 * @returns function(...args?: string, axiosConfig?: AxiosConfig{})
 * @example const getTodo = ServiceHandler({ url: '/api/todos/:id' })
 * @example await getTodo('123456');
 */
export const ServiceHandler = <Request = any, Response = AxiosResponse>(
  staticConfig = defaultStaticConfig as AxiosRequestConfig,
  instance = BackendApi as AxiosInstance
) => {
  return (...args: ServiceArgument<Request>[]) => {
    const config: AxiosRequestConfig = args.reduce(deriveHandlerConfig, {
      ...staticConfig,
      url: staticConfig.url,
    });

    return instance.request<
      Response,
      AxiosResponse<Response, Request>,
      Request
    >(config);
  };
};

function deriveHandlerConfig(
  acc: AxiosRequestConfig,
  arg: ServiceArgument,
  index: number,
  array: ServiceArgument[]
): AxiosRequestConfig {
  if (typeof arg === 'string' || typeof arg === 'number') {
    return {
      ...acc,
      url: acc.url!.replace(/:[a-z]+/i, arg as string),
    };
  }

  const isLast = index === array.length - 1;
  if (isLast && typeof arg === 'object') {
    return { ...acc, ...arg };
  }

  throw new Error(`Service handler: Invalid argument at position ${index}`);
}

/* serializers */
function standardQuerySerializer(query: any) {
  return qs.stringify(query, { arrayFormat: 'comma', addQueryPrefix: true });
}

/* request transformers */
function ignoreUndefineds(data: any) {
  return Object.keys(data).reduce((acc, key) => {
    return typeof data[key] !== 'undefined'
      ? { ...acc, [key]: data[key] }
      : acc;
  }, {});
}
