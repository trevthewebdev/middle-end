export type SelectOption =
  | string
  | {
      label: string;
      value: string | number;
    };

// export interface SelectOption {
//   label: string;
//   value: string | number;
// }
