// type Handler = <T>(data?: T) => void;

interface GenericHandlerFn<T> {
  (data?: T): void;
}

export class PubSubModule {
  topics: Record<string, GenericHandlerFn<any>[]> = {};

  /**
   * subscribe
   * @param topic {string}
   * @param listener {function}
   */
  subscribe(topic: string, handler: GenericHandlerFn<any>) {
    /* eslint-disable-next-line */
    const self = this;

    // create the topic
    if (!this.topics.hasOwnProperty.call(this.topics, topic)) {
      this.topics[topic] = [];
    }

    // add the listener to queue
    const index = this.topics[topic].push(handler) - 1;

    // provide handle back for removal of topic
    return {
      remove() {
        delete self.topics[topic][index];
      },
    };
  }

  /**
   * publish
   * @param topic {string}
   * @param data {any - default object}
   */
  publish(topic: string, data?: any) {
    // if the topic doesn't exist bail
    if (!this.topics.hasOwnProperty.call(this.topics, topic)) {
      return;
    }

    // cycle through topics queue and fire!
    this.topics[topic].forEach((handler: GenericHandlerFn<any>) => {
      handler(data);
    });
  }
}

export const PubSub = new PubSubModule();
