import styled from '@emotion/styled';

import { MantineProvider } from '@mantine/core';
import { Routes } from 'react-router-dom';

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';

import ErrorRoutes from './console/errors/errors.routes';
import HomeRoutes from './console/home/home.routes';
import RoutersRoutes from './console/routers/routers.routes';
import ScopesRoutes from './console/scopes/scopes.routes';
import TableConfigurationsRoutes from './console/table-configurations/table-configurations.routes';
import UsersRoutes from './console/users/users.routes';

const queryClient = new QueryClient();

const StyledApp = styled.div`
  // Your style here
`;

export function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <MantineProvider withGlobalStyles withNormalizeCSS>
        <Routes>
          {HomeRoutes()}
          {RoutersRoutes()}
          {TableConfigurationsRoutes()}
          {UsersRoutes()}
          {ScopesRoutes()}
          {ErrorRoutes()}
        </Routes>
      </MantineProvider>
    </QueryClientProvider>
  );
}

export default App;
