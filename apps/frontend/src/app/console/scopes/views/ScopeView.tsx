import { IScope, IUpdateScopeDto } from '@middle-end/api';

import { useQuery } from '@tanstack/react-query';
import { useParams } from 'react-router-dom';

import { Grid } from '@mantine/core';

import { ConsoleLayout } from '../../../layouts/ConsoleLayout';

import { getScope, updateScope } from '../../../api/scopes.api';
import { ScopeFormModule } from '../modules/ScopeFormModule';

interface Props {
  scope: IScope;
}

function ScopeView({ scope }: Props) {
  async function handleOnSubmit(values: IUpdateScopeDto) {
    return updateScope(scope.id, { data: values });
  }

  return (
    <ConsoleLayout justify="left">
      <Grid>
        <Grid.Col span={8}>
          <ScopeFormModule scope={scope} onSubmit={handleOnSubmit} />
        </Grid.Col>
      </Grid>
    </ConsoleLayout>
  );
}

function Resolver() {
  const { id } = useParams();

  const { data: fields, isLoading: isLoadingFields } = useQuery([id], () =>
    getScope(id as string).then((res) => res.data.data)
  );

  if (isLoadingFields) {
    return (
      <ConsoleLayout>
        <p>Loading...</p>
      </ConsoleLayout>
    );
  }

  return <ScopeView scope={fields} />;
}

export default Resolver;
