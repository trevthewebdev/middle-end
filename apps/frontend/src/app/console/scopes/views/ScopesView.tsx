import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';

import {
  ActionIcon,
  Button,
  Flex,
  Grid,
  Paper,
  Table,
  Title,
} from '@mantine/core';

import {
  faPenToSquare,
  faPlus,
  faTrash,
} from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { useQuery } from '@tanstack/react-query';
import { Link } from 'react-router-dom';

import { getScopes } from '../../../api/scopes.api';

import { ConsoleLayout } from '../../../layouts/ConsoleLayout';

function Resolver() {
  const { data, isLoading } = useQuery(['table', 'configurations'], () =>
    getScopes().then(({ data }) => data.data)
  );

  if (isLoading) {
    return <ConsoleLayout>Tables loading</ConsoleLayout>;
  }

  return <ScopesView data={data} />;
}

function ScopesView({ data }: any) {
  const table = useReactTable({
    data,
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  console.log(table.getHeaderGroups());

  return (
    <ConsoleLayout justify="left" bg="rgba(0, 0, 0, .02)">
      <Grid justify="space-between">
        <Grid.Col
          span={6}
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'flex-end',
          }}
        >
          <Title order={1}>Scopes</Title>
          <Title order={4} color="dimmed">
            List of Scopes
          </Title>
        </Grid.Col>
        <Grid.Col
          span={6}
          style={{
            display: 'flex',
            justifyContent: 'flex-end',
            alignItems: 'flex-end',
          }}
        >
          <Button
            leftIcon={<FontAwesomeIcon icon={faPlus} size="lg" />}
            component={Link}
            to="/scopes/new"
          >
            Create New Scope
          </Button>
        </Grid.Col>
        <Grid.Col span={12}>
          <Paper shadow="sm" style={{ width: '100%' }}>
            <Table
              withBorder
              highlightOnHover
              horizontalSpacing="xl"
              verticalSpacing="md"
              fontSize="md"
              bg="rgba(255, 255, 255)"
            >
              <thead>
                {table.getHeaderGroups().map((headerGroup) => (
                  <tr key={headerGroup.id}>
                    {headerGroup.headers.map((header) => (
                      <th key={header.id}>
                        {header.isPlaceholder
                          ? null
                          : flexRender(
                              header.column.columnDef.header,
                              header.getContext()
                            )}
                      </th>
                    ))}
                  </tr>
                ))}
              </thead>
              <tbody>
                {table.getRowModel().rows.map((row) => (
                  <tr key={row.id}>
                    {row.getVisibleCells().map((cell) => (
                      <td key={cell.id}>
                        {flexRender(
                          cell.column.columnDef.cell,
                          cell.getContext()
                        )}
                      </td>
                    ))}
                  </tr>
                ))}
              </tbody>
            </Table>
          </Paper>
        </Grid.Col>
      </Grid>
    </ConsoleLayout>
  );
}

export default Resolver;

const columnHelper = createColumnHelper<any>();

const columns = [
  columnHelper.accessor('scope', {}),
  columnHelper.accessor('action', {}),
  columnHelper.accessor('id', {
    header: () => null,
    cell: (row) => (
      <Flex justify="flex-end">
        <ActionIcon size="lg" component={Link} to={`/scopes/${row.getValue()}`}>
          <FontAwesomeIcon icon={faPenToSquare} />
        </ActionIcon>
        <ActionIcon size="lg" component={Link} to={`/scopes/${row.getValue()}`}>
          <FontAwesomeIcon icon={faTrash} />
        </ActionIcon>
      </Flex>
    ),
  }),
];
