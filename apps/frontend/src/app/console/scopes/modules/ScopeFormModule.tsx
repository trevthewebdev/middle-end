import { yupResolver } from '@hookform/resolvers/yup';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

import { ICreateScopeDto, IScope, IUpdateScopeDto } from '@middle-end/api';

import { Button, Flex, Grid, TextInput, Title } from '@mantine/core';

interface Props {
  scope: IScope | undefined;
  onSubmit: (values: IUpdateScopeDto) => void;
}

export const ScopeFormModule = ({ scope, onSubmit }: Props) => {
  const { register, handleSubmit } = useForm({
    defaultValues: scope,
    resolver: yupResolver(schema),
  });

  function handleOnSubmit(values: ICreateScopeDto & IUpdateScopeDto) {
    onSubmit(values);
  }

  return (
    <form onSubmit={handleSubmit(handleOnSubmit)}>
      <Grid>
        <Grid.Col span={12}>
          <Title order={1}>Scope</Title>
          <Title order={4} color="dimmed">
            Modify the scope and action below
          </Title>
        </Grid.Col>
        <Grid.Col span={6}>
          <TextInput
            label="Scope"
            placeholder="Identifier"
            {...register('scope')}
          />
        </Grid.Col>
        <Grid.Col span={6}>
          <TextInput
            label="Action"
            placeholder="Action"
            {...register('action')}
          />
        </Grid.Col>
        <Grid.Col span={12}>
          <Flex justify="space-between">
            <Button type="submit">Save Configuration</Button>
            <Button color="red" variant="subtle">
              Delete Scope
            </Button>
          </Flex>
        </Grid.Col>
      </Grid>
    </form>
  );
};

const schema = yup
  .object({
    scope: yup.string().required(),
    action: yup.string().required(),
  })
  .noUnknown(true)
  .required();
