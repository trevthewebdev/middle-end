import { Route } from 'react-router-dom';

import ScopeView from './views/ScopeView';
import ScopesView from './views/ScopesView';

export default function ScopesRoutes() {
  return [
    <Route path="/scopes/:id" element={<ScopeView />} />,
    <Route path="/scopes" element={<ScopesView />} />,
  ];
}
