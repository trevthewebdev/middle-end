import { ConsoleLayout } from '../../../layouts/ConsoleLayout';

function NotFoundView() {
  return (
    <ConsoleLayout>
      <h1>Not Found</h1>
    </ConsoleLayout>
  );
}

export default NotFoundView;
