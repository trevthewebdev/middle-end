import { Route } from 'react-router-dom';

import NotFoundView from './views/NotFoundView';

export default function ErrorRoutes() {
  return [<Route path="*" element={<NotFoundView />} />];
}
