import styled from '@emotion/styled';
import { TableColumnConfig } from '@middle-end/api';

import { Table as ManTable, Skeleton } from '@mantine/core';

interface Props {
  column: TableColumnConfig;
}

export function ColumnPreviewModule({ column }: Props) {
  return (
    <Table withColumnBorders highlightOnHover>
      <thead>
        <tr>
          <Th>
            <Skeleton animate={false} height={8} width={120} />
          </Th>
          <th>{column.title}</th>
          <Th>
            <Skeleton animate={false} height={8} width={120} />
          </Th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <Skeleton animate={false} height={8} width={120} />
          </td>
          <td>{new Date().toDateString()}</td>
          <td>
            <Skeleton animate={false} height={8} width={120} />
          </td>
        </tr>
        <tr>
          <td>
            <Skeleton animate={false} height={8} width={120} />
          </td>
          <td>{new Date().toDateString()}</td>
          <td>
            <Skeleton animate={false} height={8} width={120} />
          </td>
        </tr>
        <tr>
          <td>
            <Skeleton animate={false} height={8} width={120} />
          </td>
          <td>{new Date().toDateString()}</td>
          <td>
            <Skeleton animate={false} height={8} width={120} />
          </td>
        </tr>
      </tbody>
    </Table>
  );
}

const Table = styled(ManTable)`
  position: relative;

  &:before,
  &:after {
    content: '';
    display: block;
    width: 33.3333%;
    height: 100%;
    position: absolute;
    top: 0;
    z-index: 2;
  }

  &:before {
    left: 0;
    background: linear-gradient(
      90deg,
      rgba(255, 255, 255, 1) 33%,
      rgba(255, 255, 255, 0) 100%
    );
  }

  &:after {
    right: 0;
    background: linear-gradient(
      90deg,
      rgba(255, 255, 255, 0) 0,
      rgba(255, 255, 255, 1) 33%
    );
  }
`;

const Th = styled.th`
  text-transform: capitalize;
  color: #e1e1e1;
`;
