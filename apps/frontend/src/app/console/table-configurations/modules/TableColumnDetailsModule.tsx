import {
  StyleAlignment,
  TableColumnConfig,
  TableConfigurationEntityInterface,
} from '@middle-end/api';
import { Control, Controller, UseFormRegister } from 'react-hook-form';

import {
  Button,
  Checkbox,
  Divider,
  Grid,
  Select,
  Text,
  TextInput,
} from '@mantine/core';

import { faChevronLeft } from '@fortawesome/pro-light-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useQuery } from '@tanstack/react-query';
import { getFormatters } from '../../../api/formatters.api';
import { asSelectOptionsTransformer } from '../../../api/transformers/select-options.transformer';
import { ColumnPreviewModule } from './previews/ColumnPreviewModule';

interface Props {
  show: boolean;
  column: TableColumnConfig;
  columnIndex: number;
  control: Control<TableConfigurationEntityInterface>;
  register: UseFormRegister<TableConfigurationEntityInterface>;
  handleSelectColumn: (identifier: string | null) => () => void;
}

export function TableColumnDetailsModule({
  show = false,
  column,
  columnIndex,
  control,
  register,
  handleSelectColumn,
}: Props) {
  const { data: formatterOptions } = useQuery(
    ['formatters'],
    () => getFormatters().then((response) => response.data),
    {
      initialData: [],
      select: asSelectOptionsTransformer('name'),
    }
  );

  if (!show) {
    return null;
  }

  return (
    <Grid gutter={32}>
      <Grid.Col span={12}>
        <Button
          variant="subtle"
          onClick={handleSelectColumn(null)}
          leftIcon={<FontAwesomeIcon icon={faChevronLeft} />}
        >
          Back to columns
        </Button>
      </Grid.Col>
      <Grid.Col span={4}>
        <Grid>
          <Grid.Col span={12}>
            <TextInput
              label="Identifier"
              placeholder="e.g. modified"
              {...register(`columns.${columnIndex}.identifier`)}
            />
          </Grid.Col>
          <Grid.Col span={12}>
            <TextInput
              label="Accessor"
              description="Defaults to column identifier"
              {...register(`columns.${columnIndex}.accessor`)}
            />
          </Grid.Col>
          <Grid.Col span={12}>
            <TextInput
              label="Column header"
              {...register(`columns.${columnIndex}.title`)}
            />
          </Grid.Col>
          <Grid.Col span={12}>
            <Controller
              name={`columns.${columnIndex}.formatter`}
              control={control}
              render={({ field: { onChange, ...field } }) => (
                <Select
                  {...field}
                  onChange={(value: string) => onChange(value)}
                  label="Formatter"
                  description="Function used to format the output"
                  placeholder="e.g. as currency"
                  searchable
                  data={formatterOptions}
                />
              )}
            />
          </Grid.Col>
          <Grid.Col span={12}>
            <Checkbox label="Show by default" />
          </Grid.Col>
          <Grid.Col span={12}>
            <Checkbox label="Allow visibility toggle" />
          </Grid.Col>
          <Grid.Col span={12}>
            <Checkbox label="Can sort table by column" />
          </Grid.Col>
          <Grid.Col span={12}>
            <TextInput
              label="Empty value"
              description="What to show when no value is available"
              placeholder="e.g. '-'"
            />
          </Grid.Col>
          <Grid.Col span={12}>
            <TextInput
              label="Column width"
              type="number"
              placeholder="120"
              description="Unit is in pixels, likely will not be exact. Leave blank for automatic sizing"
              {...register(`columns.${columnIndex}.styles.width`)}
            />
          </Grid.Col>
          <Grid.Col span={12}>
            <Controller
              name={`columns.${columnIndex}.styles.align`}
              control={control}
              render={({ field: { onChange, ...field } }) => (
                <Select
                  {...field}
                  onChange={(value: StyleAlignment) => onChange(value)}
                  label="Column alignment"
                  placeholder="Left"
                  searchable
                  data={[
                    { label: 'Left', value: StyleAlignment.left },
                    { label: 'Right', value: StyleAlignment.right },
                    { label: 'Center', value: StyleAlignment.center },
                  ]}
                />
              )}
            />
          </Grid.Col>
        </Grid>
      </Grid.Col>
      <Grid.Col span={8}>
        <Grid justify="center">
          <Grid.Col span={12}>
            <ColumnPreviewModule column={column} />
          </Grid.Col>
          <Grid.Col span={8}>
            <Divider my="md" label="Column Preview" labelPosition="center" />
            <Text fz="sm" c="dimmed" align="center">
              Make adjustments to the column and see them above in real time
            </Text>
          </Grid.Col>
        </Grid>
      </Grid.Col>
    </Grid>
  );
}
