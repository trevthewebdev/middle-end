import { TableColumnConfig } from '@middle-end/api';
import { ChangeEventHandler } from 'react';

import {
  faMagnifyingGlass,
  faPen,
  faXmark,
} from '@fortawesome/pro-light-svg-icons';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import {
  ActionIcon,
  Button,
  Checkbox,
  Grid,
  Group,
  Table,
  Text,
  TextInput,
} from '@mantine/core';

interface Props {
  show: boolean;
  columns: TableColumnConfig[];
  handleSearch: ChangeEventHandler<HTMLInputElement>;
  handleSelectColumn: (identifier: string) => () => void;
  handleRemoveColumn: (identifier: string) => () => void;
}

export function TableColumnsListingModule({
  show = true,
  columns = [],
  handleSearch,
  handleSelectColumn,
  handleRemoveColumn,
}: Props) {
  if (!show) {
    return null;
  }

  return (
    <>
      <Grid>
        <Grid.Col span={10}>
          <TextInput
            placeholder="Search columns"
            size="sm"
            icon={<FontAwesomeIcon icon={faMagnifyingGlass} size="sm" />}
            styles={{ rightSection: { pointerEvents: 'none' } }}
            mb="sm"
            onChange={handleSearch}
          />
        </Grid.Col>
        <Grid.Col span={2}>
          <Button variant="outline">Add Column</Button>
        </Grid.Col>
      </Grid>
      <Table highlightOnHover>
        <thead>
          <tr>
            <th>Column header</th>
            <th>Accessor</th>
            <th>Show default</th>
            <th>Sortable</th>
            <th>Can Toggle</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {columns.map((column: TableColumnConfig, index) => (
            <tr key={column.identifier}>
              <td>{column.title}</td>
              <td>
                <Text c="dimmed">{column.accessor || column.identifier}</Text>
              </td>
              <td>
                <Checkbox size="xs" checked={column.showDefault} disabled />
              </td>
              <td>
                <Checkbox size="xs" checked={column.sortable} disabled />
              </td>
              <td>
                <Checkbox size="xs" checked={column.toggleable} disabled />
              </td>
              <td>
                <Group position="right">
                  <ActionIcon
                    variant="light"
                    size="sm"
                    onClick={handleSelectColumn(column.identifier)}
                  >
                    <FontAwesomeIcon icon={faPen} color="blue" />
                  </ActionIcon>
                  <ActionIcon
                    variant="light"
                    color="red"
                    size="sm"
                    onClick={handleRemoveColumn(column.identifier)}
                  >
                    <FontAwesomeIcon icon={faXmark} />
                  </ActionIcon>
                </Group>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
}
