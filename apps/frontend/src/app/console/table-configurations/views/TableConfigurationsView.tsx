import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';

import { Button, Table, Title } from '@mantine/core';
import { useQuery } from '@tanstack/react-query';
import { Link } from 'react-router-dom';

import { ConsoleLayout } from '../../../layouts/ConsoleLayout';

import { getTableConfigurations } from '../../../api/table-configurations.api';

function Resolver() {
  const { data, isLoading } = useQuery(['table', 'configurations'], () =>
    getTableConfigurations().then(({ data }) => data.data)
  );

  if (isLoading) {
    return <ConsoleLayout>Tables loading</ConsoleLayout>;
  }

  return <TableConfigurationsView data={data} />;
}

function TableConfigurationsView({ data }: any) {
  const table = useReactTable({
    data,
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  return (
    <ConsoleLayout>
      <Title order={1}>Tables</Title>
      <Table withBorder highlightOnHover>
        <thead>
          {table.getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => (
                <th key={header.id}>
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody>
          {table.getRowModel().rows.map((row) => (
            <tr key={row.id}>
              {row.getVisibleCells().map((cell) => (
                <td key={cell.id}>
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </Table>
    </ConsoleLayout>
  );
}

export default Resolver;

const columnHelper = createColumnHelper<any>();

const columns = [
  columnHelper.accessor('id', {
    header: () => null,
    cell: (row) => (
      <Button
        size="xs"
        variant="outline"
        component={Link}
        to={`/table-configurations/${row.getValue()}`}
      >
        Edit
      </Button>
    ),
  }),
  columnHelper.accessor('identifier', {
    header: () => null,
  }),
  columnHelper.accessor('region', {}),
  columnHelper.accessor('description', {}),
];
