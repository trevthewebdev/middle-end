import { useQuery } from '@tanstack/react-query';
import { matchSorter } from 'match-sorter';
import { ChangeEvent, useMemo, useState } from 'react';
import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { useParams } from 'react-router-dom';

import {
  TableColumnConfig,
  TableConfigurationEntityInterface,
} from '@middle-end/api';

import {
  Button,
  Divider,
  Grid,
  Select,
  SelectItem,
  TextInput,
  Textarea,
} from '@mantine/core';

import { ConsoleLayout } from '../../../layouts/ConsoleLayout';

import { getRegions } from '../../../api/regions.api';

import {
  getTableConfiguration,
  getTableConfigurationFields,
} from '../../../api/table-configurations.api';

import { asSelectOptionsTransformer } from '../../../api/transformers/select-options.transformer';
import { TableColumnDetailsModule } from '../modules/TableColumnDetailsModule';
import { TableColumnsListingModule } from '../modules/TableColumnsListingModule';

interface Props {
  fields: [];
  tableConfig: TableConfigurationEntityInterface;
  regions: SelectItem[];
}

function TableConfigurationView({ tableConfig, regions }: Props) {
  const { control, register, handleSubmit } = useForm({
    defaultValues: tableConfig,
  });

  const { fields, remove } = useFieldArray({
    control,
    name: 'columns',
  });

  function onSubmit(values: any) {
    console.log(values);
  }

  const [searchTerm, setSearchTerm] = useState('');
  function handleSearch(event: ChangeEvent<HTMLInputElement>) {
    setSearchTerm(event.target.value);
  }

  const searchableColumns: TableColumnConfig[] = useMemo(
    () => matchSorter(fields, searchTerm, { keys: ['title', 'identifier'] }),
    [fields, searchTerm]
  );

  const [column, setColumn] = useState<number | null>(null);
  function handleSelectColumn(identifier: string | null) {
    return () => {
      if (identifier === null) {
        return setColumn(null);
      }

      const nextIndex = fields.findIndex(
        (field) => field.identifier === identifier
      );

      setColumn(nextIndex);
    };
  }

  function handleRemoveColumn(identifier: string) {
    return () => {
      const nextIndex = fields.findIndex(
        (field) => field.identifier === identifier
      );

      remove(nextIndex);
    };
  }

  return (
    <ConsoleLayout justify="left">
      <h1>Table Configuration</h1>
      <p>
        Use the options below to configure the available columns that are
        allowed in the table and how they formatted.
      </p>
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid>
          <Grid.Col span={6}>
            <TextInput
              label="Identifier"
              placeholder="Identifier"
              {...register('identifier')}
            />
          </Grid.Col>
          <Grid.Col span={6}>
            <Controller
              name="region"
              control={control}
              render={({ field: { onChange, ...field } }) => (
                <Select
                  {...field}
                  onChange={(value: string) => onChange(value)}
                  label="Region"
                  placeholder="Region"
                  searchable
                  data={regions}
                />
              )}
            />
          </Grid.Col>
          <Grid.Col span={6}>
            <Textarea
              label="Description"
              placeholder="Description"
              {...register('description')}
            />
          </Grid.Col>
          <Grid.Col span={12}>
            <Button type="submit">Save Configuration</Button>
          </Grid.Col>
        </Grid>

        <Divider my="xl" label="Columns" labelPosition="center" />

        <TableColumnsListingModule
          show={column === null}
          columns={searchableColumns}
          handleSearch={handleSearch}
          handleSelectColumn={handleSelectColumn}
          handleRemoveColumn={handleRemoveColumn}
        />

        <TableColumnDetailsModule
          show={column !== null}
          column={fields[column as number]}
          columnIndex={column as number}
          control={control}
          register={register}
          handleSelectColumn={handleSelectColumn}
        />
      </form>
    </ConsoleLayout>
  );
}

function Resolver() {
  const { id } = useParams();

  const { data: fields, isLoading: isLoadingFields } = useQuery(
    ['tables', 'config', 'fields'],
    () => getTableConfigurationFields().then((res) => res.data.data)
  );

  const { data: regions, isLoading: isLoadingRegions } = useQuery(
    ['regions'],
    () => getRegions().then((res) => res.data),
    {
      select: asSelectOptionsTransformer('longName'),
    }
  );

  const { data: tableConfig, isLoading: isLoadingTableConfig } = useQuery(
    ['tables', 'config', id],
    () => getTableConfiguration(id as string).then((res) => res.data.data)
  );

  if (isLoadingTableConfig || isLoadingFields || isLoadingRegions) {
    return (
      <ConsoleLayout>
        <h1>Table Configuration</h1>
        <p>Loading...</p>
      </ConsoleLayout>
    );
  }

  return (
    <TableConfigurationView
      fields={fields}
      tableConfig={tableConfig as TableConfigurationEntityInterface}
      regions={regions as SelectItem[]}
    />
  );
}

export default Resolver;
