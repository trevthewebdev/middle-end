import { Route } from 'react-router-dom';

import TableConfigurationView from './views/TableConfigurationView';
import TableConfigurationsView from './views/TableConfigurationsView';

export default function TableConfigurationsRoutes() {
  return [
    <Route
      path="/table-configurations/:id"
      element={<TableConfigurationView />}
    />,
    <Route
      path="/table-configurations"
      element={<TableConfigurationsView />}
    />,
  ];
}
