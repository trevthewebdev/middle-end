import { Route } from 'react-router-dom';

import RouterView from './views/RouterView';
import RoutersView from './views/RoutersView';

export default function RoutersRoutes() {
  return [
    <Route path="/routes/:id" element={<RouterView />} />,
    <Route path="/routes" element={<RoutersView />} />,
  ];
}
