import { useParams } from 'react-router-dom';
import { ConsoleLayout } from '../../../layouts/ConsoleLayout';

function RouterView() {
  const { id } = useParams();

  return (
    <ConsoleLayout>
      <h1>Router {id}</h1>
    </ConsoleLayout>
  );
}

export default RouterView;
