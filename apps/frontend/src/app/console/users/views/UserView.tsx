import { ConsoleLayout } from '../../../layouts/ConsoleLayout';

function UserView() {
  return (
    <ConsoleLayout>
      <h1>User</h1>
    </ConsoleLayout>
  );
}

export default UserView;
