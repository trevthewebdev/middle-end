import { ConsoleLayout } from '../../../layouts/ConsoleLayout';

function UsersView() {
  return (
    <ConsoleLayout>
      <h1>Users</h1>
    </ConsoleLayout>
  );
}

export default UsersView;
