import { Route } from 'react-router-dom';

import UserView from './views/UserView';
import UsersView from './views/UsersView';

export default function UsersRoutes() {
  return [
    <Route path="/users/:id" element={<UsersView />} />,
    <Route path="/users" element={<UserView />} />,
  ];
}
