import {
  Container,
  Image,
  SimpleGrid,
  Text,
  ThemeIcon,
  Title,
  createStyles,
  rem,
} from '@mantine/core';
import { ConsoleLayout } from '../../../layouts/ConsoleLayout';

const useStyles = createStyles((theme) => ({
  wrapper: {
    paddingTop: rem(80),
    paddingBottom: rem(50),
  },

  item: {
    display: 'flex',
  },

  itemIcon: {
    padding: theme.spacing.xs,
    marginRight: theme.spacing.md,
  },

  itemTitle: {
    marginBottom: `calc(${theme.spacing.xs} / 2)`,
  },

  supTitle: {
    textAlign: 'center',
    textTransform: 'uppercase',
    fontWeight: 800,
    fontSize: theme.fontSizes.sm,
    color: theme.fn.variant({ variant: 'light', color: theme.primaryColor })
      .color,
    letterSpacing: rem(0.5),
  },

  title: {
    lineHeight: 1,
    textAlign: 'center',
    marginTop: theme.spacing.xl,
  },

  description: {
    textAlign: 'center',
    marginTop: theme.spacing.xs,
  },

  highlight: {
    backgroundColor: theme.fn.variant({
      variant: 'light',
      color: theme.primaryColor,
    }).background,
    padding: rem(5),
    paddingTop: 0,
    borderRadius: theme.radius.sm,
    display: 'inline-block',
    color: theme.colorScheme === 'dark' ? theme.white : 'inherit',
  },
}));

interface FeatureImage {
  image: string;
  title: React.ReactNode;
  description: React.ReactNode;
}

interface FeaturesImagesProps {
  supTitle: React.ReactNode;
  description: React.ReactNode;
  data: FeatureImage[];
}

export default function HomeView() {
  const { supTitle, description, data }: FeaturesImagesProps = mockprops;
  const { classes } = useStyles();

  const items = data.map((item) => (
    <div className={classes.item} key={item.image}>
      <ThemeIcon
        variant="light"
        className={classes.itemIcon}
        size={60}
        radius="md"
      >
        {/* <Image src={IMAGES[item.image]} /> */}
        <Image />
      </ThemeIcon>

      <div>
        <Text fw={700} fz="lg" className={classes.itemTitle}>
          {item.title}
        </Text>
        <Text c="dimmed">{item.description}</Text>
      </div>
    </div>
  ));

  return (
    <ConsoleLayout>
      <Container size={700} className={classes.wrapper}>
        <Text className={classes.supTitle}>{supTitle}</Text>

        <Title className={classes.title} order={2}>
          Giving you <span className={classes.highlight}>control</span> for the
          success of Company.
        </Title>

        <Container size={660} p={0}>
          <Text color="dimmed" className={classes.description}>
            {description}
          </Text>
        </Container>

        <SimpleGrid
          cols={2}
          spacing={50}
          breakpoints={[{ maxWidth: 550, cols: 1, spacing: 40 }]}
          style={{ marginTop: 30 }}
        >
          {items}
        </SimpleGrid>
      </Container>
    </ConsoleLayout>
  );
}

const mockprops = {
  supTitle: 'Middle end',
  description:
    'Get started here to configure the bits of the ui you care about. Add columns to tables, change date range defaults, and update localized languages all in one place.',
  data: [
    {
      image: 'auditors',
      title: 'Routing',
      description: 'Dynamic & editable routes, keeping everything in sync',
    },
    {
      image: 'lawyers',
      title: 'Table configurations',
      description: 'Exactly what you want to see, how you want to see it',
    },
    {
      image: 'accountants',
      title: 'Date ranges',
      description: 'React to feedback, and market demands',
    },
    // {
    //   image: 'others',
    //   title: 'Others',
    //   description: 'Phanpy uses its long nose to shower itself',
    // },
  ],
};
