import { Route } from 'react-router-dom';

import HomeView from './views/HomeView';

export default function HomeRoutes() {
  return [<Route path="/" element={<HomeView />} />];
}
