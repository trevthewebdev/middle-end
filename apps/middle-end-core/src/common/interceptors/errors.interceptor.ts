import {
  BadRequestException,
  CallHandler,
  ConflictException,
  ExecutionContext,
  Injectable,
  NestInterceptor,
} from '@nestjs/common';
import { Observable, catchError, throwError } from 'rxjs';
import {
  PG_INVALID_INPUT_SYNTAX_ERROR_CODE,
  PG_UNIQUE_VIOLATION_ERROR_CODE,
} from '../pg.constants';

@Injectable()
export class ErrorsInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    return next.handle().pipe(
      catchError((error) => {
        /* Handle unique error (conflicts) from the database */
        if (error.code === PG_UNIQUE_VIOLATION_ERROR_CODE) {
          throw new ConflictException(error.message);
        }

        /* Handle invalid uuid error from the database */
        if (error.code === PG_INVALID_INPUT_SYNTAX_ERROR_CODE) {
          throw new BadRequestException([error.message]);
        }

        return throwError(() => error);
      })
    );
  }
}
