export enum Portal {
  cowbell = 'cowbell',
  agent = 'agent',
  insured = 'insured',
  mssp = 'mssp',
  carrier = 'carrier',
  aggregators = 'aggregators',
}
