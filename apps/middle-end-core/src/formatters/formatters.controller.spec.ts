import { Test, TestingModule } from '@nestjs/testing';
import { FormattersController } from './formatters.controller';
import { FormattersService } from './formatters.service';

describe('FormattersController', () => {
  let controller: FormattersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FormattersController],
      providers: [FormattersService],
    }).compile();

    controller = module.get<FormattersController>(FormattersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
