import { FormatterType } from '@middle-end/api';
import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsNotEmpty, IsOptional } from 'class-validator';

export class CreateFormatterDto {
  @ApiProperty()
  @IsNotEmpty()
  name: string;

  @ApiProperty()
  @IsNotEmpty()
  description: string;

  @ApiProperty({
    enum: FormatterType,
  })
  @IsEnum(FormatterType)
  @IsNotEmpty()
  type: FormatterType;

  @ApiProperty()
  @IsOptional()
  options: any;
}
