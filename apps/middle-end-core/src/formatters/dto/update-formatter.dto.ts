import { PartialType } from '@nestjs/swagger';
import { CreateFormatterDto } from './create-formatter.dto';

export class UpdateFormatterDto extends PartialType(CreateFormatterDto) {}
