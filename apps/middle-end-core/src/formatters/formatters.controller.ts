import {
  ApiBadRequestResponse,
  ApiBody,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';

import { PaginationQueryDto } from '../common/dto/pagination-query.dto';
import { CreateFormatterDto } from './dto/create-formatter.dto';
import { UpdateFormatterDto } from './dto/update-formatter.dto';

import { FormattersService } from './formatters.service';

@Controller('formatters')
@ApiTags('formatters')
export class FormattersController {
  constructor(private readonly formattersService: FormattersService) {}

  @Post()
  @ApiCreatedResponse()
  @ApiBadRequestResponse()
  @ApiBody({ type: CreateFormatterDto })
  create(@Body() createFormatterDto: CreateFormatterDto) {
    return this.formattersService.create(createFormatterDto);
  }

  @Get()
  @ApiOkResponse()
  findAll(@Query() query: PaginationQueryDto) {
    return this.formattersService.findAll(query);
  }

  @Get(':id')
  @ApiOkResponse()
  @ApiNotFoundResponse()
  findOne(@Param('id') id: string) {
    return this.formattersService.findOne(id);
  }

  @Patch(':id')
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  @ApiBody({ type: UpdateFormatterDto })
  update(
    @Param('id') id: string,
    @Body() updateFormatterDto: UpdateFormatterDto
  ) {
    return this.formattersService.update(id, updateFormatterDto);
  }

  @Delete(':id')
  @ApiNotFoundResponse()
  remove(@Param('id') id: string) {
    return this.formattersService.remove(id);
  }
}
