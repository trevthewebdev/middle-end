import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Formatter } from './entities/formatter.entity';
import { FormattersController } from './formatters.controller';
import { FormattersService } from './formatters.service';

@Module({
  imports: [TypeOrmModule.forFeature([Formatter])],
  controllers: [FormattersController],
  providers: [FormattersService],
})
export class FormattersModule {}
