import { FormatterEntityInterface, FormatterType } from '@middle-end/api';
import { Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity('formatters')
@Unique(['name', 'type'])
export class Formatter implements FormatterEntityInterface {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  description: string;

  @Column({
    type: 'enum',
    enum: FormatterType,
    default: FormatterType.custom,
  })
  type: FormatterType;

  @Column({
    type: 'jsonb',
    array: false,
    default: () => "'{}'",
    nullable: false,
  })
  options: string;
}
