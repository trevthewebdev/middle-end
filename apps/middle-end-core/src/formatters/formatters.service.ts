import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { Formatter } from './entities/formatter.entity';

import { PaginationQueryDto } from '../common/dto/pagination-query.dto';
import { CreateFormatterDto } from './dto/create-formatter.dto';
import { UpdateFormatterDto } from './dto/update-formatter.dto';

@Injectable()
export class FormattersService {
  constructor(
    @InjectRepository(Formatter)
    private readonly formatterRepo: Repository<Formatter>
  ) {}

  async create(body: CreateFormatterDto) {
    const formatter = await this.formatterRepo.create(body);
    return this.formatterRepo.save(formatter);
  }

  findAll(query: PaginationQueryDto) {
    const { limit, offset } = query;

    return this.formatterRepo.find({
      skip: offset,
      take: limit,
    });
  }

  async findOne(id: string) {
    const formatter = await this.formatterRepo.findOne({
      where: { id },
    });

    if (!formatter) {
      throw new NotFoundException(`Formatter ${id} not found`);
    }

    return formatter;
  }

  async update(id: string, body: UpdateFormatterDto) {
    const formatter = await this.formatterRepo.preload({ id, ...body });

    if (!formatter) {
      throw new NotFoundException(`Formatter ${id} not found`);
    }

    return this.formatterRepo.save(formatter);
  }

  async remove(id: string) {
    const formatter = await this.findOne(id);

    if (!formatter) {
      throw new NotFoundException(`Formatter ${id} not found`);
    }

    return this.formatterRepo.remove(formatter);
  }
}
