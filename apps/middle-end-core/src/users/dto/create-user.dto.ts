import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsEmail, IsString, IsUUID } from 'class-validator';

export class CreateUserDto {
  @ApiProperty()
  @IsString()
  firstName: string;

  @ApiProperty()
  @IsString()
  lastName: string;

  @ApiProperty({
    example: 'john@gmail.com',
  })
  @IsEmail()
  email: string;

  @ApiProperty()
  @IsBoolean()
  enabled?: boolean;

  @ApiProperty()
  @IsUUID()
  roleId: string;
}
