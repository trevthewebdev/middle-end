import {
  ConflictException,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { User } from './entities/user.entity';

import { PaginationQueryDto } from '../common/dto/pagination-query.dto';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly userRepo: Repository<User> // @InjectRepository(RouteNavigation) // private readonly routeNavigationRepo: Repository<RouteNavigation>
  ) {}

  async create(body: CreateUserDto) {
    const existingUser = await this.exists(body.email);

    if (existingUser) {
      throw new ConflictException({
        errors: [
          {
            title: 'User already exists',
            detail: `User already exists at ${existingUser.id}`,
            links: [`/api/users/${existingUser.id}`],
          },
        ],
      });
    }

    const user = await this.userRepo.create(body);
    return this.userRepo.save(user);
  }

  findAll(query: PaginationQueryDto) {
    const { limit, offset } = query;

    return this.userRepo.find({
      skip: offset,
      take: limit,
    });
  }

  async findOne(id: string) {
    const user = await this.userRepo.findOne({
      where: { id },
      relations: ['role'],
    });

    if (!user) {
      throw new NotFoundException(`User ${id} not found`);
    }

    return user;
  }

  async update(id: string, body: UpdateUserDto) {
    const user = await this.userRepo.preload({ id, ...body });

    if (!user) {
      throw new NotFoundException(`User ${id} not found`);
    }

    return this.userRepo.save(user);
  }

  async remove(id: string) {
    const user = await this.findOne(id);

    if (!user) {
      throw new NotFoundException(`User ${id} not found`);
    }

    return this.userRepo.remove(user);
  }

  async invite() {
    throw new NotImplementedException();
  }

  async exists(identifier: string) {
    // is the identifier an email?
    const where =
      identifier.indexOf('@') !== -1
        ? { email: identifier }
        : { id: identifier };

    const user = await this.userRepo.findOne({
      where,
    });

    return user;
  }
}
