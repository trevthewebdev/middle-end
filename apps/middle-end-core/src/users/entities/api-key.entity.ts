import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';

import { Exclude } from 'class-transformer';
import { Region } from '../../regions/entities/region.entity';

@Entity('api_keys')
@Unique(['env', 'region'])
export class ApiKey {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Exclude()
  @Column()
  key: string;

  @Exclude()
  @Column({ unique: true })
  uuid: string;

  @Column()
  env: string;

  @ManyToOne(() => Region, (region) => region.apiKey)
  @JoinColumn()
  region: string;
}
