import { registerAs } from '@nestjs/config';
import { randomBytes } from 'crypto';

export default registerAs('cipher', () => {
  return {
    iv: randomBytes(16),
    algorithm: process.env.CIPHER_ALGORITHM || 'aes-256-ctr',
    secretKey: process.env.CIPHER_SECRET_KEY,
  };
});
