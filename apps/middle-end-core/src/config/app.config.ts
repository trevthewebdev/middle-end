export default () => ({
  environment: process.env.NODE_ENV || 'development',
  database: {
    type: 'postgres',
    host: process.env.DATABASE_HOST,
    port: parseInt(process.env.DATABASE_PORT, 10) || 5432,
    database: process.env.DATABASE_NAME,
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,

    /* typeorm specific */

    // models will be loaded automatically
    autoLoadEntities: true,

    // your entities will be synced with the database(recommended: disable in prod)
    synchronize: process.env.NODE_ENV === 'production' ? false : true,
    migrations: ['dist/src/db/migrations/*.js'],
    cli: {
      migrationsDir: 'src/db/migrations',
    },
  },
});
