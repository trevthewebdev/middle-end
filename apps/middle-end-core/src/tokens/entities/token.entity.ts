import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('tokens')
// @TableInheritance({ column: { name: '_type', type: 'varchar' } })
export class Token {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  token: string;

  @Column({ type: 'timestamp' })
  expires: Date;
}
