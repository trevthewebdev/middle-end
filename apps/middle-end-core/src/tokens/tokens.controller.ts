import { Controller, Delete, Get, Param, Query } from '@nestjs/common';

import { ApiNotFoundResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';

import { TokensService } from './tokens.service';

import { Auth } from '../iam/authentication/decorators/auth.decorator';
import { AuthType } from '../iam/authentication/enums/auth-type.enum';

import { PaginationQueryDto } from '../common/dto/pagination-query.dto';

@ApiTags('tokens')
@Auth(AuthType.None)
@Controller('tokens')
export class TokensController {
  constructor(private readonly tokensService: TokensService) {}

  @Get()
  @ApiOkResponse()
  findAll(@Query() query: PaginationQueryDto) {
    return this.tokensService.findAll(query);
  }

  @Get(':id')
  @ApiOkResponse()
  @ApiNotFoundResponse()
  async findOne(@Param('id') id: string) {
    return this.tokensService.findOne(id);
  }

  @Delete(':id')
  @ApiNotFoundResponse()
  remove(@Param('id') id: string) {
    return this.tokensService.remove(id);
  }
}
