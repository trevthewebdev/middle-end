import {
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ConfigType } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

import dayjs from 'dayjs';
import isSameOrBeforePlugin from 'dayjs/plugin/isSameOrBefore';

import cipherConfig from '../config/cipher.config';
import jwtConfig from '../iam/config/jwt.config';

import { Token } from './entities/token.entity';

import { createCipheriv, createDecipheriv, randomUUID } from 'crypto';
import { PaginationQueryDto } from '../common/dto/pagination-query.dto';

dayjs.extend(isSameOrBeforePlugin);

interface MagicLogin {
  userId: string;
}

@Injectable()
export class TokensService {
  constructor(
    @InjectRepository(Token)
    private readonly tokenRepo: Repository<Token>,
    @Inject(jwtConfig.KEY)
    private readonly jwtConfiguration: ConfigType<typeof jwtConfig>,
    private readonly jwtService: JwtService,
    @Inject(cipherConfig.KEY)
    private readonly cipherConfiguration: ConfigType<typeof cipherConfig>
  ) {}

  async create(body: MagicLogin) {
    const now = dayjs();
    const expires = now.add(5, 'minutes');
    const nextRecordId = randomUUID();
    const { algorithm, secretKey, iv } = this.cipherConfiguration;

    const cipher = createCipheriv(algorithm, secretKey, iv);

    const value = `${nextRecordId}:${body.userId}`;
    const encrypted = Buffer.concat([cipher.update(value), cipher.final()]);

    const tokenRecord = await this.tokenRepo.create({
      ...body,
      id: nextRecordId,
      expires: new Date(expires.unix() * 1000),
      token: encrypted.toString('hex'),
    });

    return this.tokenRepo.save(tokenRecord);
  }

  async findAll(query: PaginationQueryDto) {
    const { limit, offset } = query;

    return this.tokenRepo.find({
      skip: offset,
      take: limit,
    });
  }

  async findOne(id: string) {
    const token = await this.tokenRepo.findOne({
      where: { id },
    });

    if (!token) {
      throw new NotFoundException(`Region ${id} not found`);
    }

    return token;
  }

  async remove(id: string) {
    const token = await this.findOne(id);

    if (!token) {
      throw new NotFoundException(`Region ${id} not found`);
    }

    return this.tokenRepo.remove(token);
  }

  async issueAccessToken(user) {
    const accessToken = await this.jwtService.signAsync(
      {
        sub: user.id,
        email: user.email,
        roleId: user.role.id,
      },
      {
        audience: this.jwtConfiguration.audience,
        issuer: this.jwtConfiguration.issuer,
        secret: this.jwtConfiguration.secret,
        expiresIn: this.jwtConfiguration.accessTokenTtl,
      }
    );

    return accessToken;
  }

  async validateShortToken(token: string): Promise<{
    valid: boolean;
    tokenRecord?: Token;
    payload?: { [key: string]: string };
  }> {
    const { algorithm, secretKey, iv } = this.cipherConfiguration;
    const decipher = createDecipheriv(algorithm, secretKey, Buffer.from(iv));
    const decrpyted = Buffer.concat([
      decipher.update(Buffer.from(token, 'hex')),
      decipher.final(),
    ]);

    let tokenId = null;
    let userId = null;

    try {
      const tokensstring = decrpyted.toString();
      const [_tokenId, _userId] = tokensstring.split(':');
      tokenId = _tokenId;
      userId = _userId;
    } catch (error) {
      throw new UnauthorizedException();
    }

    try {
      const tokenRecord = await this.findOne(tokenId);
      const tokenHasExpired = await this.checkExpiration(
        tokenRecord.expires.valueOf()
      );

      console.log(tokenHasExpired);
      console.log(tokenRecord.expires.valueOf());

      if (!tokenRecord || tokenHasExpired) {
        return { valid: false };
      }

      return { valid: true, tokenRecord, payload: { userId } };
    } catch (error) {
      return { valid: false };
    }
  }

  async verify(accessToken): Promise<object> {
    return this.jwtService.verify(accessToken);
  }

  async checkExpiration(expires: number) {
    const now = dayjs();
    return dayjs(expires).isSameOrBefore(now);
  }

  async decode() {
    return null;
  }
}
