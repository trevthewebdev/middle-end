import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaginationQueryDto } from '../common/dto/pagination-query.dto';
import { CreateRegionDto } from './dto/create-region.dto';
import { UpdateRegionDto } from './dto/update-region.dto';
import { Region } from './entities/region.entity';

@Injectable()
export class RegionsService {
  constructor(
    @InjectRepository(Region)
    private readonly regionRepo: Repository<Region>
  ) {}

  async create(body: CreateRegionDto) {
    const region = await this.regionRepo.create(body);
    return this.regionRepo.save(region);
  }

  async findAll(query: PaginationQueryDto) {
    const { limit, offset } = query;

    return this.regionRepo.find({
      skip: offset,
      take: limit,
    });
  }

  async findOne(id: string) {
    const region = await this.regionRepo.findOne({
      where: { id },
    });

    if (!region) {
      throw new NotFoundException(`Region ${id} not found`);
    }

    return region;
  }

  async update(id: string, body: UpdateRegionDto) {
    const region = await this.regionRepo.preload({ id, ...body });

    if (!region) {
      throw new NotFoundException(`Region ${id} not found`);
    }

    return this.regionRepo.save(region);
  }

  async remove(id: string) {
    const region = await this.findOne(id);

    if (!region) {
      throw new NotFoundException(`Region ${id} not found`);
    }

    return this.regionRepo.remove(region);
  }
}
