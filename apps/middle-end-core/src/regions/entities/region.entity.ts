import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

import { TableConfiguration } from '../../tables/entities/table-configurations.entity';
import { ApiKey } from '../../users/entities/api-key.entity';

@Entity('regions')
export class Region {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  shortName: string;

  @Column()
  longName: string;

  @Column({ unique: true })
  countryCode: string;

  @OneToMany(() => TableConfiguration, (tableConfig) => tableConfig.region, {
    nullable: true,
  })
  tableConfiguration: string;

  @OneToMany(() => ApiKey, (apiKey) => apiKey.region, { nullable: true })
  apiKey: string;
}
