import dotenv from 'dotenv';
import 'reflect-metadata';
import { DataSource, DataSourceOptions } from 'typeorm';
import { SeederOptions, runSeeders } from 'typeorm-extension';

import { Role } from '../iam/roles/entities/role.entity';
import { Route } from '../routes/entities/route.entity';
import { Scope } from '../scopes/entities/scope.entity';
import { User } from '../users/entities/user.entity';

import { RoleFactory } from './factories/role.factory';
import { RouteFactory } from './factories/route.factory';
import { SystemScopesFactory } from './factories/system-scope.factory';
import { UserFactory } from './factories/user.factory';

import { Region } from '../regions/entities/region.entity';
import { TableConfiguration } from '../tables/entities/table-configurations.entity';
import { RegionFactory } from './factories/region.factory';
import { TableConfigFactory } from './factories/table.factory';

import { Formatter } from '../formatters/entities/formatter.entity';
import { ApiKey } from '../users/entities/api-key.entity';
import { ApiKeyFactory } from './factories/api-keys.factory';
import { FormatterFactory } from './factories/formatter.factory';
import ApiKeySeeder from './seeders/api-keys.seeder';
import FormatterSeeder from './seeders/formatter.seeder';
import RegionsSeeder from './seeders/regions.seeder';
import RoutesSeeder from './seeders/routes.seeder';
import SystemRolesSeeder from './seeders/system-roles.seeder';
import SystemScopesSeeder from './seeders/system-scopes.seeder';
import SystemUsersSeeder from './seeders/system-users.seeder';
import TableConfigurationsSeeder from './seeders/table-configurations.seeder';

dotenv.config();

const options: DataSourceOptions & SeederOptions = {
  type: 'postgres',
  host: process.env.DATABASE_HOST,
  port: Number(process.env.DATABASE_PORT),
  username: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASSWORD,
  database: process.env.DATABASE_NAME,
  entities: [
    Region,
    ApiKey,
    Scope,
    User,
    Role,
    Route,
    Formatter,
    TableConfiguration,
  ],

  /* additional config options brought by typeorm-extension */
  factories: [
    SystemScopesFactory,
    RoleFactory,
    UserFactory,
    RouteFactory,
    RegionFactory,
    FormatterFactory,
    TableConfigFactory,
    ApiKeyFactory,
  ],
  seeds: [
    RegionsSeeder,
    SystemScopesSeeder,
    SystemRolesSeeder,
    SystemUsersSeeder,
    RoutesSeeder,
    FormatterSeeder,
    TableConfigurationsSeeder,
    ApiKeySeeder,
  ],
  parallelExecution: false,
};

const dataSource = new DataSource(options);

dataSource.initialize().then(async () => {
  await dataSource.synchronize(true);
  await runSeeders(dataSource);

  process.exit();
});
