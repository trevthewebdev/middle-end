export default [
  {
    id: '859448e5-7f4f-4f78-9365-8ddb5b868be8',
    longName: 'United States',
    shortName: 'US',
    countryCode: 'US',
  },
  {
    id: 'ea956c03-dc92-4852-9382-34907fd23d7e',
    longName: 'United Kingdom',
    shortName: 'UK',
    countryCode: 'GB',
  },
];
