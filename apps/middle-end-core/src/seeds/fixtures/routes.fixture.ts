import { Portal } from '../../common/enums/portal.enum';

import SystemScopes from './system-scopes.fixture';

const [Root, ManageAccounts] = SystemScopes;

export default [
  {
    id: '99abbd08-75e6-4d4b-821f-ab8a7ceeda8e',
    path: '/accounts',
    exact: true,
    portal: Portal.cowbell,
    view: 'CowbellAccountListing',
    scopes: [Root],
  },
  {
    id: 'f819027d-c03d-435c-84aa-edd19df45368',
    path: '/accounts/:id',
    exact: true,
    portal: Portal.cowbell,
    view: 'CowbellAccountDetails',
    scopes: [Root],
  },
  {
    id: 'b69540a0-c4a6-4358-a99f-39826893f33b',
    path: '/quotes',
    exact: true,
    portal: Portal.cowbell,
    view: 'CowbellQuotesListing',
    scopes: [Root],
  },
  {
    id: 'e65f12ea-e2ba-41c6-8600-e4f41f621cb1',
    path: '/quotes/:id',
    exact: true,
    portal: Portal.cowbell,
    view: 'CowbellQuoteDetails',
    scopes: [Root],
  },
  {
    id: '5534ee4e-5b66-4f03-8723-97fbc8328fd2',
    path: '/policies',
    exact: true,
    portal: Portal.cowbell,
    view: 'CowbellPoliciesListing',
    scopes: [Root],
  },
  {
    id: '7d15d666-e3d3-416a-9da8-6be0b0ecd682',
    path: '/policies/:id',
    exact: true,
    portal: Portal.cowbell,
    view: 'CowbellPolicyDetails',
    scopes: [Root],
  },
  {
    id: 'd25849fe-8ce9-4f8b-866b-92d6314b16ca',
    path: '/accounts',
    exact: true,
    portal: Portal.agent,
    view: 'AgentAccountsListing',
    scopes: [ManageAccounts],
  },
  {
    id: '44b19862-145e-440a-80f1-d16ae50341fb',
    path: '/accounts/:id',
    exact: true,
    portal: Portal.agent,
    view: 'AgentAccountDetails',
    scopes: [ManageAccounts],
  },
];
