export default [
  {
    id: 'd406a397-5257-421e-8d76-190abf7dc577',
    scope: 'root',
    action: 'root',
  },
  {
    id: 'fa5f35e6-8dbb-4761-b36c-874b352c1df3',
    scope: 'accounts',
    action: 'manage',
  },
];
