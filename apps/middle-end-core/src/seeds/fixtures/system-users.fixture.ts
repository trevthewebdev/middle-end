import SystemRolesFixture from './system-roles.fixture';

const [SystemAdmin, CustomerService, LeadUnderwriter] = SystemRolesFixture;

export default [
  {
    firstName: 'Trevor',
    lastName: 'Pierce',
    email: 'trevor@gmail.com',
    role: SystemAdmin,
  },
  {
    firstName: 'David',
    lastName: 'Humes',
    email: 'david@gmail.com',
    role: LeadUnderwriter,
  },
  {
    firstName: 'Sanka',
    lastName: 'Sopas',
    email: 'sanka@gmail.com',
    role: CustomerService,
  },
];
