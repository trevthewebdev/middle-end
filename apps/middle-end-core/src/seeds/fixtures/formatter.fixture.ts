import { FormatterType } from '@middle-end/api';

export default [
  {
    id: 'ddbecad4-f164-4933-9b20-2f5e4c1a02bc',
    name: 'Currency',
    description: 'Display number as currency',
    type: FormatterType.number,
    options: '{}',
  },
  {
    id: '811a49c7-8548-434e-9b14-6956f5162a4e',
    name: 'Abbv. Currency',
    description: 'Display number as abbreviated currency e.g. 1M',
    type: FormatterType.number,
    options: '{}',
  },
  {
    id: '384ef069-66a0-4557-bdb6-0070a449fb7a',
    name: 'GMT Date',
    description: 'Display date as GMT date',
    type: FormatterType.date,
    options: '{}',
  },
  {
    id: 'f687b1a8-0379-4025-b866-a307ccab32d6',
    name: 'Local Date & Time',
    description: "Display date & time in user's local timezone",
    type: FormatterType.date,
    options: '{}',
  },
  {
    id: 'a683c8d7-e86d-4d8c-a3b5-afe5fa7cbda4',
    name: 'Linked Account Name',
    description: 'Display value as a link',
    type: FormatterType.custom,
    options: '{}',
  },
];
