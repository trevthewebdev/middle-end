export default [
  {
    identifier: 'cbid',
    title: 'CBID',
    showDefault: false,
    sortable: true,
    toggleable: true,
  },
  {
    identifier: 'favourite',
    title: 'Favorite',
    showDefault: true,
    sortable: false,
    toggleable: true,
  },
  {
    identifier: 'isPaperApp',
    title: 'App',
    showDefault: true,
    sortable: false,
    toggleable: true,
    style: {
      align: 'center',
      width: 90,
    },
  },
  {
    identifier: 'name',
    title: 'Account Name',
    showDefault: true,
    sortable: true,
    toggleable: false,
    fixed: 'left',
    style: {
      width: 'l',
    },
  },
  {
    identifier: 'quickQuotePremium',
    accessor: 'rater.quickQuotePremium',
    title: 'Indicated Premium',
    showDefault: true,
    sortable: true,
    toggleable: true,
    style: {
      align: 'right',
      width: 's',
    },
    getCellValue: 'asCurrency', // needs to move to enum
  },
];
