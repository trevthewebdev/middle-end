import RegionsFixture from './regions.fixture';

const [US, UK] = RegionsFixture;

export default [
  {
    id: '46f72d7e-b2e3-4cb3-be6a-604cd512613b',
    key: '$2b$10$9sp0YtNayPNG77g2hkPHrO1A0.HArM673CgNf2gt9Q3Y3xMN2EiNO',
    uuid: '650abdde-e4b3-4e86-abbd-a24e7191f44b',
    env: 'devint',
    region: US.id,
  },
  {
    id: 'e4798399-1006-4e92-8672-8e7872b9f447',
    key: '$2b$10$v5p3hRIw0TD7L7Xqb6E7tuXXtacM/ICoo2RpXSuO1RoxD71eacAFi',
    uuid: '3baba531-9bb1-4015-b938-f8eca0c150ea',
    env: 'staging',
    region: US.id,
  },
  {
    id: '460875ed-0f57-40ae-944f-da8e605b78d2',
    key: '$2b$10$J0742wKjUGfwkaLa7WLAxOmpNdd54zUDRTe96oE.kPoqxeNXzFanW',
    uuid: '277b0154-a43c-4377-abc4-bf1e68811092',
    env: 'staging',
    region: UK.id,
  },
];
