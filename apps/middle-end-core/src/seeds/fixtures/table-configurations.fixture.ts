import RegionsFixture from './regions.fixture';
import CowbellAccountColumnsFixture from './table-configurations/cowbell-account-columns.fixture';

const [US, UK] = RegionsFixture;

export default [
  {
    id: '5c681d5c-b537-467d-afc6-ffe85a92f136',
    identifier: 'COWBELL_ACCOUNTS',
    region: US.id,
    columns: CowbellAccountColumnsFixture,
    description: 'Columns configuration for table at {{env[region][url]}}',
  },
  {
    id: '91dd81f0-6cdd-45fc-8cf5-cf3aabf9466c',
    identifier: 'COWBELL_QUOTES',
    region: US.id,
    columns: CowbellAccountColumnsFixture,
    description: 'Columns configuration for table at {{env[region][url]}}',
  },
  {
    id: 'fed25c4e-35a0-42f0-a41a-2703c5fa51c8',
    identifier: 'COWBELL_POLICIES',
    region: US.id,
    columns: CowbellAccountColumnsFixture,
    description: 'Columns configuration for table at {{env[region][url]}}',
  },
  {
    id: '53def650-02f1-4ebc-b1e4-6ed648abb9f6',
    identifier: 'COWBELL_POLICIES',
    region: UK.id,
    columns: CowbellAccountColumnsFixture,
    description: 'Columns configuration for table at {{env[region][url]}}',
  },
];
