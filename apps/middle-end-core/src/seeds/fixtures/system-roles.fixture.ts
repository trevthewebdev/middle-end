export default [
  {
    id: '918a0251-1f60-4c0b-85d5-022bc10a58ff',
    name: 'System: Admin',
  },
  {
    id: '4b05c8c9-ab92-410f-afa6-34ff052a5639',
    name: 'System: Customer Service',
  },
  {
    id: '68fe47ec-5e85-4d56-836c-0ba336aa5849',
    name: 'System: Lead Underwriter',
  },
];
