import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

import { Logger } from '@nestjs/common';

import { TableConfiguration } from '../../tables/entities/table-configurations.entity';
import TableConfigFixtures from '../fixtures/table-configurations.fixture';

export default class TableConfigurationsSeeder implements Seeder {
  private readonly logger = new Logger(TableConfigurationsSeeder.name);

  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
  ): Promise<void> {
    const factory = factoryManager.get(TableConfiguration);

    this.logger.debug(`${TableConfigurationsSeeder.name} Started`);

    return await Promise.all(
      TableConfigFixtures.map((fixture) => factory.save(fixture as any))
    )
      .then((results) =>
        this.logger.debug(`Seeded ${results.length} table configurations`)
      )
      .finally(() =>
        this.logger.log(`${TableConfigurationsSeeder.name} completed`)
      );
  }
}
