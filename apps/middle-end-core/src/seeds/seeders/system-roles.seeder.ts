import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

import { Logger } from '@nestjs/common';
import { Role } from '../../iam/roles/entities/role.entity';
import SystemRolesFixture from '../fixtures/system-roles.fixture';

export default class SystemRolesSeeder implements Seeder {
  private readonly logger = new Logger(SystemRolesSeeder.name);

  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
  ): Promise<void> {
    const factory = factoryManager.get(Role);

    this.logger.debug(`${SystemRolesSeeder.name} Started`);

    return await Promise.all(
      SystemRolesFixture.map((fixture) => factory.save(fixture))
    )
      .then((results) => this.logger.debug(`Seeded ${results.length} roles`))
      .finally(() => this.logger.log(`${SystemRolesSeeder.name} completed`));
  }
}
