import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

import { Logger } from '@nestjs/common';

import { Region } from '../../regions/entities/region.entity';
import RegionsFixture from '../fixtures/regions.fixture';

export default class RegionsSeeder implements Seeder {
  private readonly logger = new Logger(RegionsSeeder.name);

  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
  ): Promise<void> {
    const factory = factoryManager.get(Region);

    this.logger.debug(`${RegionsSeeder.name} Started`);

    return await Promise.all(
      RegionsFixture.map((fixture) => factory.save(fixture))
    )
      .then((results) => this.logger.debug(`Seeded ${results.length} regions`))
      .finally(() => this.logger.log(`${RegionsSeeder.name} completed`));
  }
}
