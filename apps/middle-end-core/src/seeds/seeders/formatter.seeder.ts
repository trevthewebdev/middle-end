import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

import { Logger } from '@nestjs/common';
import { Formatter } from '../../formatters/entities/formatter.entity';
import FormatterFixture from '../fixtures/formatter.fixture';

export default class FormatterSeeder implements Seeder {
  private readonly logger = new Logger(FormatterSeeder.name);

  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
  ): Promise<void> {
    const factory = factoryManager.get(Formatter);

    this.logger.debug(`${FormatterSeeder.name} Started`);

    return await Promise.all(
      FormatterFixture.map((fixture) => factory.save(fixture))
    )
      .then((results) =>
        this.logger.debug(`Seeded ${results.length} formatters`)
      )
      .finally(() => this.logger.log(`${FormatterSeeder.name} completed`));
  }
}
