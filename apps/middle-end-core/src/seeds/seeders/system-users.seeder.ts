import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

import { Logger } from '@nestjs/common';
import { User } from '../../users/entities/user.entity';
import SystemUsersFixtures from '../fixtures/system-users.fixture';

export default class SystemUsersSeeder implements Seeder {
  private readonly logger = new Logger(SystemUsersSeeder.name);

  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
  ): Promise<void> {
    const factory = factoryManager.get(User);

    this.logger.debug(`${SystemUsersSeeder.name} Started`);

    return await Promise.all(
      SystemUsersFixtures.map((fixture) => factory.save(fixture))
    )
      .then((results) => this.logger.debug(`Seeded ${results.length} users`))
      .finally(() => this.logger.log(`${SystemUsersSeeder.name} completed`));
  }
}
