import { Logger } from '@nestjs/common';
import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

import { ApiKey } from '../../users/entities/api-key.entity';

import ApiKeyFixture from '../fixtures/api-keys.fixture';

export default class ApiKeySeeder implements Seeder {
  private readonly logger = new Logger(ApiKeySeeder.name);

  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
  ): Promise<void> {
    const factory = factoryManager.get(ApiKey);

    this.logger.debug(`${ApiKeySeeder.name} Started`);

    return await Promise.all(
      ApiKeyFixture.map((fixture) => factory.save(fixture))
    )
      .then((results) => this.logger.debug(`Seeded ${results.length} api keys`))
      .finally(() => this.logger.log(`${ApiKeySeeder.name} completed`));
  }
}
