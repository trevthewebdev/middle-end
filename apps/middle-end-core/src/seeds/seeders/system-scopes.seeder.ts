import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

import { Logger } from '@nestjs/common';

import { Scope } from '../../scopes/entities/scope.entity';
import SystemScopesFixtures from '../fixtures/system-scopes.fixture';

export default class SystemScopesSeeder implements Seeder {
  private readonly logger = new Logger(SystemScopesSeeder.name);

  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
  ): Promise<void> {
    const factory = factoryManager.get(Scope);

    this.logger.debug(`${SystemScopesSeeder.name} Started`);

    return await Promise.all(
      SystemScopesFixtures.map((fixture) => factory.save(fixture))
    )
      .then((results) => {
        this.logger.debug(`Seeded ${results.length} system scopes`);
      })
      .finally(() => this.logger.log(`${SystemScopesSeeder.name} completed`));
  }
}
