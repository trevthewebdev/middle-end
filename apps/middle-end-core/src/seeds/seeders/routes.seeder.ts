import { DataSource } from 'typeorm';
import { Seeder, SeederFactoryManager } from 'typeorm-extension';

import { Logger } from '@nestjs/common';

import { Route } from '../../routes/entities/route.entity';
import RoutesFixtures from '../fixtures/routes.fixture';

export default class RoutesSeeder implements Seeder {
  private readonly logger = new Logger(RoutesSeeder.name);

  public async run(
    dataSource: DataSource,
    factoryManager: SeederFactoryManager
  ): Promise<void> {
    const factory = factoryManager.get(Route);

    this.logger.debug(`${RoutesSeeder.name} Started`);

    return await Promise.all(
      RoutesFixtures.map((fixture) => factory.save(fixture))
    )
      .then((results) => this.logger.debug(`Seeded ${results.length} routes`))
      .finally(() => this.logger.log(`${RoutesSeeder.name} completed`));
  }
}
