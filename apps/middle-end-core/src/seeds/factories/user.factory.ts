import { Faker } from '@faker-js/faker';
import { setSeederFactory } from 'typeorm-extension';

import { User } from '../../users/entities/user.entity';

export const UserFactory = setSeederFactory(User, (faker: Faker) => {
  const firstName = faker.person.firstName('male');
  const lastName = faker.person.lastName('male');

  const user = new User();
  user.firstName = firstName;
  user.lastName = lastName;

  user.email = `${firstName}.${lastName}@gmail.com`.toLocaleLowerCase();

  return user;
});
