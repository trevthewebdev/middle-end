import { Faker } from '@faker-js/faker';
import { setSeederFactory } from 'typeorm-extension';

import { Region } from '../../regions/entities/region.entity';

export const RegionFactory = setSeederFactory(Region, (faker: Faker) => {
  const region = new Region();

  region.shortName = faker.person.jobArea();
  region.longName = faker.person.jobArea();
  region.countryCode = faker.person.jobArea();

  return region;
});
