import { Faker } from '@faker-js/faker';
import { setSeederFactory } from 'typeorm-extension';
import { Scope } from '../../scopes/entities/scope.entity';

export const SystemScopesFactory = setSeederFactory(Scope, (faker: Faker) => {
  const scope = new Scope();

  // route.name = faker.person.jobArea();
  scope.scope = 'fake';
  scope.action = 'scope';

  return scope;
});
