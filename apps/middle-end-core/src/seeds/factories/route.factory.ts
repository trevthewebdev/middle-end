import { Faker } from '@faker-js/faker';
import { setSeederFactory } from 'typeorm-extension';
import { Portal } from '../../common/enums/portal.enum';
import { Route } from '../../routes/entities/route.entity';

export const RouteFactory = setSeederFactory(Route, (faker: Faker) => {
  const route = new Route();

  // route.name = faker.person.jobArea();
  route.path = faker.person.jobDescriptor();
  route.exact = true;
  route.portal = Portal.mssp;
  route.scopes = [];
  route.view = faker.person.jobDescriptor();

  return route;
});
