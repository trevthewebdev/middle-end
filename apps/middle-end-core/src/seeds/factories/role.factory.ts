import { Faker } from '@faker-js/faker';
import { setSeederFactory } from 'typeorm-extension';

import { Role } from '../../iam/roles/entities/role.entity';

export const RoleFactory = setSeederFactory(Role, (faker: Faker) => {
  const role = new Role();

  role.name = faker.person.jobArea();

  return role;
});
