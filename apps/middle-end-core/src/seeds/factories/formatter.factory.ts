import { Faker } from '@faker-js/faker';
import { FormatterType } from '@middle-end/api';
import { setSeederFactory } from 'typeorm-extension';
import { Formatter } from '../../formatters/entities/formatter.entity';

export const FormatterFactory = setSeederFactory(Formatter, (faker: Faker) => {
  const formatter = new Formatter();

  formatter.name = faker.person.jobArea();
  formatter.options = faker.person.jobArea();
  formatter.description = faker.person.jobArea();

  formatter.type = FormatterType.custom;

  return formatter;
});
