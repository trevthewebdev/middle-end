import { Faker } from '@faker-js/faker';
import { setSeederFactory } from 'typeorm-extension';

import { ApiKey } from '../../users/entities/api-key.entity';

export const ApiKeyFactory = setSeederFactory(ApiKey, (faker: Faker) => {
  const apiKey = new ApiKey();

  apiKey.key = faker.person.jobArea();
  apiKey.uuid = faker.person.jobArea();
  apiKey.env = faker.person.jobArea();
  apiKey.region = faker.person.jobArea();

  return apiKey;
});
