import { Faker } from '@faker-js/faker';
import { setSeederFactory } from 'typeorm-extension';

import { TableConfiguration } from '../../tables/entities/table-configurations.entity';

export const TableConfigFactory = setSeederFactory(
  TableConfiguration,
  (faker: Faker) => {
    const table = new TableConfiguration();

    table.identifier = faker.person.jobArea();
    table.region = 'US';
    table.columns = [];

    return table;
  }
);
