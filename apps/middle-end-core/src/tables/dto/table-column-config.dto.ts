import {
  StyleAlignment,
  TableColumnConfig,
  TableColumnStyles,
} from '@middle-end/api';
import { IsBoolean, IsEnum, IsOptional, IsString } from 'class-validator';

export class TableColumnConfigDto implements TableColumnConfig {
  @IsString()
  identifier: string;

  @IsString()
  @IsOptional()
  accessor?: string;

  @IsString()
  title: string;

  @IsBoolean()
  showDefault?: boolean;

  @IsOptional()
  @IsEnum(StyleAlignment)
  fixed?: StyleAlignment;

  @IsBoolean()
  sortable?: boolean;

  @IsBoolean()
  toggleable?: boolean;

  @IsString()
  @IsOptional()
  emptyValue?: string;

  @IsOptional()
  styles?: TableColumnStyles;

  @IsString()
  @IsOptional()
  formatter?: string;
}
