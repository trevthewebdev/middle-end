import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsArray,
  IsOptional,
  IsString,
  IsUUID,
  ValidateNested,
} from 'class-validator';
import { TableColumnConfigDto } from './table-column-config.dto';

export class CreateTableDto {
  @ApiProperty({ example: 'COWBELL_ACCOUNTS' })
  @IsString()
  identifier: string;

  @ApiProperty()
  @IsUUID()
  region: string;

  @ApiProperty({
    example: [
      {
        identifier: 'name',
        title: 'Account Name',
        showDefault: true,
        sortable: true,
        toggleable: false,
        fixed: 'left',
        style: {
          width: 'l',
        },
      },
    ],
  })
  @IsArray()
  @Type(() => TableColumnConfigDto)
  @ValidateNested({ each: true })
  columns: TableColumnConfigDto[];

  @ApiProperty()
  @IsString()
  @IsOptional()
  description?: string;
}
