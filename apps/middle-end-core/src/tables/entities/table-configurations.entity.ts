import {
  TableColumnConfig,
  TableConfigurationEntityInterface,
} from '@middle-end/api';

import {
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';

import { Region } from '../../regions/entities/region.entity';

@Entity('table_configurations')
@Unique(['identifier', 'region'])
export class TableConfiguration implements TableConfigurationEntityInterface {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  identifier: string;

  @ManyToOne(() => Region, (region) => region.tableConfiguration)
  @JoinColumn()
  region: string;

  @Column({ type: 'jsonb' })
  columns: TableColumnConfig[];

  @Column({ nullable: true })
  description?: string;
}
