import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PaginationQueryDto } from '../common/dto/pagination-query.dto';
import { CreateTableDto } from './dto/create-table.dto';
import { UpdateTableDto } from './dto/update-table.dto';

import { TableConfiguration } from './entities/table-configurations.entity';

@Injectable()
export class TablesService {
  constructor(
    @InjectRepository(TableConfiguration)
    private readonly tableConfigRepo: Repository<TableConfiguration>
  ) {}

  async create(body: CreateTableDto) {
    console.log('body: ', body);
    const tableConfig = await this.tableConfigRepo.create(body);
    return this.tableConfigRepo.save(tableConfig);
  }

  async findAll(query: PaginationQueryDto) {
    const { limit, offset } = query;

    return this.tableConfigRepo.find({
      skip: offset,
      take: limit,
    });
  }

  async findOne(id: string) {
    const tableConfig = await this.tableConfigRepo.findOne({
      where: { id },
    });

    if (!tableConfig) {
      throw new NotFoundException(`Table configuration ${id} not found`);
    }

    return tableConfig;
  }

  async update(id: string, body: UpdateTableDto) {
    const tableConfig = await this.tableConfigRepo.preload({ id, ...body });

    if (!tableConfig) {
      throw new NotFoundException(`Table configuration ${id} not found`);
    }

    return this.tableConfigRepo.save(tableConfig);
  }

  async remove(id: string) {
    const tableConfig = await this.findOne(id);

    if (!tableConfig) {
      throw new NotFoundException(`Table configuration ${id} not found`);
    }

    return this.tableConfigRepo.remove(tableConfig);
  }
}
