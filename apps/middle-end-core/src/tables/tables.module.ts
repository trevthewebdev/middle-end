import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { TableConfiguration } from './entities/table-configurations.entity';
import { TablesController } from './tables.controller';
import { TablesService } from './tables.service';

@Module({
  imports: [TypeOrmModule.forFeature([TableConfiguration])],
  controllers: [TablesController],
  providers: [TablesService],
})
export class TablesModule {}
