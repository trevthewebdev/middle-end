export default [
  {
    name: 'identifier',
    description: 'Identifies the column. Ensure this is unique',
  },
  {
    name: 'accessor',
    description:
      'The property in the response where the value is stored. Defaults to the "identifier"',
  },
  { name: 'title', description: "The text to display in the column's header" },
  { name: 'showDefault', description: 'Show this column by default' },
  {
    name: 'fixed',
    description: 'Affixes the column to the left or right of all columns',
  },
  { name: 'sortable', description: 'Allows for sorting on this columns data' },
  {
    name: 'toggleable',
    description: 'Allows for toggling visibility of this column',
  },
  {
    name: 'emptyValue',
    description:
      'Shows a fallback value if no value was found using the "accessor". Defaults to "-"',
  },
  { name: 'styles.align', description: 'Text alignment within the cell' },
  { name: 'styles.width', description: 'Relative width of the cell' },
];
