import { Column, Entity, PrimaryGeneratedColumn, Unique } from 'typeorm';

@Entity('user-preferences')
@Unique(['userId', 'portal'])
export class UserPreference {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  userId: string;

  @Column()
  portal: string;
}
