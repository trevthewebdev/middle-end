import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CreateRouteDto } from './dto/create-route.dto';
import { UpdateRouteDto } from './dto/update-route.dto';

import { PaginationQueryDto } from '../common/dto/pagination-query.dto';
import { Route } from './entities/route.entity';

@Injectable()
export class RoutesService {
  constructor(
    @InjectRepository(Route)
    private readonly routeRepo: Repository<Route> // @InjectRepository(RouteNavigation) // private readonly routeNavigationRepo: Repository<RouteNavigation>
  ) {}

  async create(body: CreateRouteDto) {
    const existingRoute = await this.exists(body);

    if (existingRoute) {
      throw new ConflictException({
        errors: [
          {
            title: 'Route already exists',
            detail: `Route already exists at ${existingRoute.id}`,
            links: [`/api/routes/${existingRoute.id}`],
          },
        ],
      });
    }

    const route = await this.routeRepo.create(body);
    return this.routeRepo.save(route);
  }

  findAll(query: PaginationQueryDto) {
    const { limit, offset } = query;

    return this.routeRepo.find({
      skip: offset,
      take: limit,
    });
  }

  async findOne(id: string) {
    const route = await this.routeRepo.findOne({
      where: { id },
    });

    if (!route) {
      throw new NotFoundException(`Route ${id} not found`);
    }

    return route;
  }

  async update(id: string, body: UpdateRouteDto) {
    const route = await this.routeRepo.preload({ id, ...body });

    if (!route) {
      throw new NotFoundException(`Route ${id} not found`);
    }

    return this.routeRepo.save(route);
  }

  async remove(id: string) {
    const route = await this.findOne(id);

    if (!route) {
      throw new NotFoundException(`Route ${id} not found`);
    }

    return this.routeRepo.remove(route);
  }

  exists(createRouteDto: CreateRouteDto) {
    return this.routeRepo.findOne({
      where: {
        path: createRouteDto.path,
        portal: createRouteDto.portal,
      },
    });
  }
}
