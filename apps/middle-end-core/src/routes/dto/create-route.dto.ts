import { ApiProperty } from '@nestjs/swagger';
import { IsBoolean, IsEnum, IsNotEmpty, IsString } from 'class-validator';
import { Portal } from '../../common/enums/portal.enum';
import { Scope } from '../../scopes/entities/scope.entity';

export class CreateRouteDto {
  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  path: string;

  @ApiProperty()
  @IsBoolean()
  exact: boolean;

  @ApiProperty({
    enum: Portal,
  })
  @IsEnum(Portal)
  @IsNotEmpty()
  portal: Portal;

  @ApiProperty()
  @IsString()
  @IsNotEmpty()
  view: string;

  @ApiProperty()
  scopes: Scope[];
}
