import {
  Column,
  Entity,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { Route } from './route.entity';

@Entity('routes')
export class RouteNavigation {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @OneToOne(() => Route, (route) => route.path)
  @JoinColumn()
  path: Route;

  @Column()
  name: string;

  @Column({ nullable: true })
  icon?: string;

  @Column()
  portal: string;

  // scopes: [];
}
