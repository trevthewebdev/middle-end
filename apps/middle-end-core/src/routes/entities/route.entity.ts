import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';

import { Portal } from '../../common/enums/portal.enum';
import { Scope } from '../../scopes/entities/scope.entity';

@Entity('routes')
export class Route {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  path: string;

  @Column()
  exact: boolean;

  @Column({
    type: 'enum',
    enum: Portal,
    default: Portal.cowbell,
  })
  portal: Portal;

  @Column()
  view: string;

  @Column({ nullable: true })
  featureFlag?: string;

  @JoinTable({ name: 'routes_scopes' })
  @ManyToMany(() => Scope, (scope) => scope.routes, { nullable: true })
  scopes: Scope[];
}
