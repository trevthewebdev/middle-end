import { Test, TestingModule } from '@nestjs/testing';
import { RoutesService } from './routes.service';

import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Route } from './entities/route.entity';

describe('RoutesService', () => {
  let service: RoutesService;
  let repository: Repository<Route>;

  const REPO_TOKEN = getRepositoryToken(Route);

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        RoutesService,
        {
          provide: REPO_TOKEN,
          useValue: {
            create: jest.fn(),
            findAll: jest.fn(),
            findOne: jest.fn(),
          },
        },
      ],
    }).compile();

    service = module.get<RoutesService>(RoutesService);
    repository = module.get<Repository<Route>>(REPO_TOKEN);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
