import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { PaginationQueryDto } from '../../common/dto/pagination-query.dto';
import { CreateRoleDto } from './dto/create-role.dto';
import { UpdateRoleDto } from './dto/update-role.dto';
import { Role } from './entities/role.entity';

@Injectable()
export class RolesService {
  constructor(
    @InjectRepository(Role)
    private readonly rolesRepo: Repository<Role>
  ) {}

  async create(createRoleDto: CreateRoleDto) {
    const user = await this.rolesRepo.create(createRoleDto);
    return this.rolesRepo.save(user);
  }

  async findAll(paginationQuery: PaginationQueryDto) {
    const { limit, offset } = paginationQuery;

    return this.rolesRepo.find({
      skip: offset,
      take: limit,
    });
  }

  async findOne(id: string) {
    const user = await this.rolesRepo.findOne({
      where: { id },
    });

    if (!user) {
      throw new NotFoundException(`User ${id} not found`);
    }

    return user;
  }

  async update(id: string, updateRoleDto: UpdateRoleDto) {
    const user = await this.rolesRepo.preload({ id, ...updateRoleDto });

    if (!user) {
      throw new NotFoundException(`User ${id} not found`);
    }

    return this.rolesRepo.save(user);
  }

  async remove(id: string) {
    const user = await this.findOne(id);
    return this.rolesRepo.remove(user);
  }
}
