import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

import { PaginationQueryDto } from '../../common/dto/pagination-query.dto';
import { ApiKeysService } from './api-keys.service';
import { Auth } from './decorators/auth.decorator';
import { CreateApiKeyDto } from './dtos/create-api-key.dto';
import { AuthType } from './enums/auth-type.enum';

@ApiTags('api keys')
@Auth(AuthType.Bearer)
@Controller('api-keys')
export class ApiKeysController {
  constructor(private readonly apiKeyService: ApiKeysService) {}

  @Post()
  @ApiCreatedResponse()
  @ApiBadRequestResponse()
  @ApiConflictResponse()
  create(@Body() createUserDto: CreateApiKeyDto) {
    return this.apiKeyService.create(createUserDto);
  }

  @Get()
  @ApiOkResponse()
  findAll(@Query() query: PaginationQueryDto) {
    return this.apiKeyService.findAll(query);
  }

  @Get(':id')
  @ApiOkResponse()
  @ApiNotFoundResponse()
  findOne(@Param('id') id: string) {
    return this.apiKeyService.findOne(id);
  }

  @Delete(':id')
  @ApiNotFoundResponse()
  remove(@Param('id') id: string) {
    return this.apiKeyService.remove(id);
  }
}
