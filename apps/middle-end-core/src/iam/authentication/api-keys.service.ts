import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { randomUUID } from 'crypto';
import { Repository } from 'typeorm';

import { ApiKey } from '../../users/entities/api-key.entity';

import { PaginationQueryDto } from '../../common/dto/pagination-query.dto';
import { HashingService } from '../hashing/hashing.service';
import { GeneratedApiKeyPayload } from '../interfaces/generate-api-key-payload.interface';
import { CreateApiKeyDto } from './dtos/create-api-key.dto';

@Injectable()
export class ApiKeysService {
  constructor(
    @InjectRepository(ApiKey)
    private readonly apiKeyRepo: Repository<ApiKey>,
    private readonly hashingService: HashingService
  ) {}

  async create(body: CreateApiKeyDto) {
    const nextuuid = randomUUID();
    const { apiKey, hashedKey } = await this.createAndHash(nextuuid, [
      body.env,
      body.region,
    ]);

    /* todo:  */
    const record = await this.apiKeyRepo.create({
      ...body,
      key: hashedKey,
      uuid: nextuuid,
    });

    await this.apiKeyRepo.save(record);

    /* todo: need to leverage class transformer to move excludes to the entity  */
    const { key, uuid, ...payload } = { ...record };
    return { ...payload, apiKey };
  }

  findAll(query: PaginationQueryDto) {
    const { limit, offset } = query;

    return this.apiKeyRepo.find({
      skip: offset,
      take: limit,
      relations: ['region'],
    });
  }

  async findOne(id: string) {
    const apikey = await this.apiKeyRepo.findOne({
      where: { id },
      relations: ['region'],
    });

    if (!apikey) {
      throw new NotFoundException(`Api Key ${id} not found`);
    }

    return apikey;
  }

  async remove(id: string) {
    const apikey = await this.findOne(id);

    if (!apikey) {
      throw new NotFoundException(`Api Key ${id} not found`);
    }

    return this.apiKeyRepo.remove(apikey);
  }

  async createAndHash(
    id: string,
    payload?: string[]
  ): Promise<GeneratedApiKeyPayload> {
    const apiKey = this.generateApiKey(id, payload);

    /**
     * Note: we're using bcrypt here which is slower on purpose
     * bcrypt might not be required here
     */
    const hashedKey = await this.hashingService.hash(apiKey);

    return { apiKey, hashedKey };
  }

  async validate(apiKey: string, hashedKey: string): Promise<boolean> {
    return this.hashingService.compare(apiKey, hashedKey);
  }

  extractIdFromApiKey(apiKey: string): string {
    const [id] = Buffer.from(apiKey, 'base64').toString('ascii').split(' ');
    return id;
  }

  private generateApiKey(id: string, payload: string[] = []): string {
    const apiKey = `${id} ${randomUUID()} ${payload.join(' ')}`.trimEnd();
    return Buffer.from(apiKey).toString('base64');
  }
}
