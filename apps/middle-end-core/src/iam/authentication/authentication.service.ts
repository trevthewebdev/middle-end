import {
  Inject,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { ConfigType } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

import jwtConfig from '../config/jwt.config';

import { User } from '../../users/entities/user.entity';

import { TokensService } from '../../tokens/tokens.service';

import { SignInDto } from './dtos/sign-in.dto';

@Injectable()
export class AuthenticationService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    @Inject(jwtConfig.KEY)
    private readonly jwtConfiguration: ConfigType<typeof jwtConfig>,
    private readonly jwtService: JwtService,
    private readonly tokenService: TokensService
  ) {}

  async requestSignIn(body: SignInDto) {
    const user = await this.userRepo.findOne({ where: { email: body.email } });

    if (!user) {
      throw new NotFoundException();
    }

    const token = await this.tokenService.create({ userId: user.id });

    /* todo: send an email with link to the frontend */
    if (process.env.NODE_ENV === 'LOCAL') {
      return token;
    }
  }

  async confirmSignIn(token: string) {
    const { valid, payload } = await this.tokenService.validateShortToken(
      token
    );

    const user = await this.userRepo.findOne({
      where: { id: payload.userId },
      relations: ['role'],
    });

    if (!valid) {
      throw new UnauthorizedException('Invalid token');
    }

    return this.tokenService.issueAccessToken(user);
  }

  async sendMagicLink() {
    return null;
  }
}
