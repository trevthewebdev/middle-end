import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { ApiNoContentResponse, ApiTags } from '@nestjs/swagger';

import dayjs from 'dayjs';
import isSameOrBeforePlugin from 'dayjs/plugin/isSameOrBefore';

import { Auth } from './decorators/auth.decorator';
import { AuthType } from './enums/auth-type.enum';

import { AuthenticationService } from './authentication.service';

import { SignInDto } from './dtos/sign-in.dto';

dayjs.extend(isSameOrBeforePlugin);

@ApiTags('authentication')
@Auth(AuthType.None)
@Controller('authentication')
export class AuthenticationController {
  constructor(private readonly authService: AuthenticationService) {}

  @Post('sign-in')
  @ApiNoContentResponse()
  signIn(@Body() body: SignInDto) {
    return this.authService.requestSignIn(body);
  }

  @Post('sign-in/:token')
  async confirmSignIn(@Param('token') shortToken: string) {
    const accessToken = await this.authService.confirmSignIn(shortToken);
    return { accessToken };
  }

  @Get('date')
  async getDate() {
    const now = dayjs();
    const expires = now.add(5, 'minutes');

    return new Date(expires.unix() * 1000).toDateString();
  }
}
