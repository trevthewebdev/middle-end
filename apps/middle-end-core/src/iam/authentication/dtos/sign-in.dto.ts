import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';

export class SignInDto {
  @ApiProperty({ example: 'jonny@gmail.com' })
  @IsEmail()
  email: string;
}
