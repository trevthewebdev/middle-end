import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsUUID } from 'class-validator';

export class CreateApiKeyDto {
  @ApiProperty({ example: 'devint' })
  @IsNotEmpty()
  env: string;

  @ApiProperty({
    example: '859448e5-7f4f-4f78-9365-8ddb5b868be8',
    description: 'id of a region',
  })
  @IsUUID()
  region: string;
}
