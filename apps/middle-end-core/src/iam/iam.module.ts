import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Token } from '../tokens/entities/token.entity';
import { ApiKey } from '../users/entities/api-key.entity';
import { User } from '../users/entities/user.entity';
import { Role } from './roles/entities/role.entity';

import { TokensModule } from '../tokens/tokens.module';

import { ApiKeysController } from './authentication/api-keys.controller';
import { ApiKeysService } from './authentication/api-keys.service';
import { AuthenticationController } from './authentication/authentication.controller';
import { AuthenticationService } from './authentication/authentication.service';
import { BcryptService } from './hashing/bcrypt.service';
import { HashingService } from './hashing/hashing.service';

import { AccessTokenGuard } from './authentication/guards/access-token.guard';
import { ApiKeyGuard } from './authentication/guards/api-key.guard';

import jwtConfig from './config/jwt.config';

@Module({
  imports: [
    TypeOrmModule.forFeature([User, Role, ApiKey, Token]),
    JwtModule.registerAsync(jwtConfig.asProvider()),
    ConfigModule.forFeature(jwtConfig),
    TokensModule,
  ],
  providers: [
    {
      provide: HashingService,
      useClass: BcryptService,
    },
    // {
    //   provide: APP_GUARD,
    //   useClass: AuthenticationGuard,
    // },
    ApiKeysService,
    AccessTokenGuard,
    ApiKeyGuard,
    AuthenticationService,
  ],
  controllers: [AuthenticationController, ApiKeysController],
})
export class IamModule {}
