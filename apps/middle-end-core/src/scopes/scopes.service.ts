import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaginationQueryDto } from '../common/dto/pagination-query.dto';
import { CreateScopeDto } from './dto/create-scope.dto';
import { UpdateScopeDto } from './dto/update-scope.dto';
import { Scope } from './entities/scope.entity';

@Injectable()
export class ScopesService {
  constructor(
    @InjectRepository(Scope)
    private readonly scopeRepo: Repository<Scope>
  ) {}

  async create(body: CreateScopeDto) {
    const scope = await this.scopeRepo.create(body);
    return this.scopeRepo.save(scope);
  }

  async findAll(query: PaginationQueryDto) {
    const { limit, offset } = query;

    return this.scopeRepo.find({
      skip: offset,
      take: limit,
    });
  }

  async findOne(id: string) {
    const scope = await this.scopeRepo.findOne({
      where: { id },
    });

    if (!scope) {
      throw new NotFoundException(`Scope ${id} not found`);
    }

    return scope;
  }

  async update(id: string, body: UpdateScopeDto) {
    const scope = await this.scopeRepo.preload({ id, ...body });

    if (!scope) {
      throw new NotFoundException(`Scope ${id} not found`);
    }

    return this.scopeRepo.save(scope);
  }

  async remove(id: string) {
    const scope = await this.findOne(id);

    if (!scope) {
      throw new NotFoundException(`Scope ${id} not found`);
    }

    return this.scopeRepo.remove(scope);
  }
}
