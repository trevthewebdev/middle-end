import { ICreateScopeDto } from '@middle-end/api';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty } from 'class-validator';

export class CreateScopeDto implements ICreateScopeDto {
  @ApiProperty()
  @IsNotEmpty()
  scope: string;

  @ApiProperty()
  @IsNotEmpty()
  action: string;
}
