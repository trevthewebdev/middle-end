import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConflictResponse,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger';

import { PaginationQueryDto } from '../common/dto/pagination-query.dto';
import { CreateScopeDto } from './dto/create-scope.dto';
import { UpdateScopeDto } from './dto/update-scope.dto';
import { ScopesService } from './scopes.service';

@ApiTags('scopes')
@Controller('scopes')
export class ScopesController {
  constructor(private readonly scopesService: ScopesService) {}

  @Post()
  @ApiCreatedResponse()
  @ApiBadRequestResponse()
  @ApiConflictResponse()
  create(@Body() createScopeDto: CreateScopeDto) {
    return this.scopesService.create(createScopeDto);
  }

  @Get()
  @ApiOkResponse()
  findAll(@Query() query: PaginationQueryDto) {
    return this.scopesService.findAll(query);
  }

  @Get(':id')
  @ApiOkResponse()
  @ApiNotFoundResponse()
  findOne(@Param('id') id: string) {
    return this.scopesService.findOne(id);
  }

  @Patch(':id')
  @ApiNotFoundResponse()
  @ApiBadRequestResponse()
  update(@Param('id') id: string, @Body() updateScopeDto: UpdateScopeDto) {
    return this.scopesService.update(id, updateScopeDto);
  }

  @Delete(':id')
  @ApiNotFoundResponse()
  remove(@Param('id') id: string) {
    return this.scopesService.remove(id);
  }
}
