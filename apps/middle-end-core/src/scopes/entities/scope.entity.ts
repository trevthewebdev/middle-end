import {
  Column,
  Entity,
  ManyToMany,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';

import { Route } from '../../routes/entities/route.entity';

@Entity('scopes')
@Unique(['scope', 'action'])
export class Scope {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  scope: string;

  @Column()
  action: string;

  @ManyToMany(() => Route, (route) => route.scopes, { nullable: true })
  routes?: Route[];
}
