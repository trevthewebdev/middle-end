import { Module } from '@nestjs/common';

import * as Joi from '@hapi/joi';

import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CommonModule } from './common/common.module';
import appConfig from './config/app.config';
import { FormattersModule } from './formatters/formatters.module';
import { HealthModule } from './health/health.module';
import { IamModule } from './iam/iam.module';
import { RegionsModule } from './regions/regions.module';
import { RoutesModule } from './routes/routes.module';
import { ScopesModule } from './scopes/scopes.module';
import { TablesModule } from './tables/tables.module';
import { UserPreferencesModule } from './user-preferences/user-preferences.module';
import { User } from './users/entities/user.entity';
import { UsersModule } from './users/users.module';
import { TokensModule } from './tokens/tokens.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [appConfig],
      validationSchema: Joi.object({
        DATABASE_HOST: Joi.required(),
        DATABASE_PORT: Joi.number().default(5432),
      }),
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (config: ConfigService) => config.get('database'),
    }),
    IamModule,
    UsersModule,
    User,
    UserPreferencesModule,
    CommonModule,
    HealthModule,
    RoutesModule,
    ScopesModule,
    TablesModule,
    RegionsModule,
    FormattersModule,
    TokensModule,
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
