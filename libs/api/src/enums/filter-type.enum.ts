export enum FilterType {
  string = 'string',
  number = 'number',
  boolean = 'boolean',
  list = 'list',
  date = 'date',
}
