export enum FormatterType {
  string = 'string',
  boolean = 'boolean',
  number = 'number',
  custom = 'custom',
  date = 'date',
}
