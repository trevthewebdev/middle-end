export enum StyleAlignment {
  left = 'left',
  right = 'right',
  center = 'center',
}
