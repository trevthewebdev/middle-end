export interface DateFormatterOptions {
  format: string;
}

export interface NumberFormatterOptions {
  format: string;
}
