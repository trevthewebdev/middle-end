import { FormatterType } from '../../../enums/formatter-type.enum';

export interface FormatterEntityInterface {
  id: string;
  name: string;
  description: string;
  options: string;
  type: FormatterType;
}
