export interface IScope {
  id: string;
  scope: string;
  action: string;
}
