export interface ICreateScopeDto {
  scope: string;
  action: string;
}
