export interface IUpdateScopeDto {
  scope?: string;
  action?: string;
}
