// dtos
export * from './dtos/create-scope.dto.interface';
export * from './dtos/update-scope.dto.interface';
export * from './scope.entity.interface';
