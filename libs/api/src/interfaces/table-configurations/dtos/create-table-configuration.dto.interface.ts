import { TableColumnConfig } from '../table-configuration-column.interface';

export interface CreateTableConfigurationDtoInterface {
  identifier: string;
  region: string;
  columns: Array<TableColumnConfig>;
  description?: string;
}
