import { StyleAlignment } from '../../enums/style-alignment.enum';
import { StyleSize } from '../../enums/style-size.enum';

export interface TableColumnStyles {
  align?: StyleAlignment;
  width?: StyleSize;
}
