// entities
export * from './entities/table-configuration.entity.interface';

// dtos

// interfaces
export * from './table-column-styles.interface';
export * from './table-configuration-column.interface';
export * from './table-filter.interface';
