import { FilterType } from '../../enums/filter-type.enum';

export interface TableFilter {
  identifier: string;
  type: FilterType;
}
