import { TableColumnConfig } from '../table-configuration-column.interface';

export interface TableConfigurationEntityInterface {
  id: string;
  identifier: string;
  region: string;
  columns: Array<TableColumnConfig>;
  description?: string;
}
