import { StyleAlignment } from '../../enums/style-alignment.enum';
import { TableColumnStyles } from './table-column-styles.interface';

export interface TableColumnConfig {
  title: string;
  identifier: string;
  accessor?: string;
  showDefault?: boolean;
  fixed?: StyleAlignment;
  sortable?: boolean;
  toggleable?: boolean;
  emptyValue?: string;
  styles?: TableColumnStyles;
  formatter?: string;
}
