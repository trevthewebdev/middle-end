export interface RegionDtoInterface {
  id: string;
  shortName: string;
  longName: string;
  countryCode: string;
}
