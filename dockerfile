FROM node:16-alpine AS builder
WORKDIR /app
COPY . .
RUN yarn install && \
    npx nx build middle-end-core --production && \
    npx nx build frontend --production

FROM node:16-alpine
WORKDIR /app
COPY --from=builder ./app/dist ./dist
COPY yarn.lock .
RUN yarn install --production
CMD [ "npx", "nx", "serve", "middle-end-core" ]
