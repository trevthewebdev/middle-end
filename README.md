# MiddleEnd

## Prepare for local development

### Prerequisites
1. Docker desktop is install and daemon is running
2. You're using node `18.15.x` or higher (nvm is recommended on Mac OSX)

This guide assumes the following...
1. You've cloned the repository and have changed into the project's directory.
2. You have yarn installed globally (recommended)

Copy .env file for injection of environment variables, the example file contents have good enough defaults for local development
```
cp example.env .env
```

Start local infrastructure via docker e.g. databases etc.
```
docker compose up -d
```

Add icon library token
```
echo "@fortawesome:registry=https://npm.fontawesome.com/
//npm.fontawesome.com/:_authToken=371F780D-4113-427C-8292-056C1437C6CC
" >> .npmrc
```

Install dependencies
```
yarn
```

## Development server

### nx command not installed globally

Backend
```
npx nx serve middle-end-core
```

Frontend
```
npx nx serve frontend
```

### nx command is installed globally
Backend
```
nx serve middle-end-core
```

Frontend
```
nx serve frontend
```

## Want some seed data?

Run the following then kill the process when it completes
```
npx nx run middle-end-core:seed
```

Backend available at http://localhost:6100/api

Frontend available at http://localhost:4200/

Swagger docs are available at http://localhost:6100/docs/api

---
Below are barrowed from the nx starting docs

## Understand this workspace

Run `nx graph` to see a diagram of the dependencies of the projects.

## Remote caching

Run `npx nx connect-to-nx-cloud` to enable [remote caching](https://nx.app) and make CI faster.

## Further help

Visit the [Nx Documentation](https://nx.dev) to learn more.
